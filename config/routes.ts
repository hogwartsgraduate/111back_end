// config/routes.ts

export default [
  {
    path: '/',
    redirect: '/home',
  },
  {
    exact: true,
    path: '/home',
    component: 'index',
    wrappers: ['@/wrappers/isToken'],
    name: '首页',
  },
  {
    path: '/login',
    component: 'login',
    icon: 'testicon',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },

  // 产品管理  新增和编辑页面
  {
    exact: true,
    path: '/prodInfo',
    component: '@/pages/prod/prodList/prodInfo.tsx',
    wrappers: ['@/wrappers/isToken'],
  },
];
