// config/config.ts

import { defineConfig } from 'umi';
import routes from './routes';
// import logo111 from '../src/static/logo.png';

export default defineConfig({
  routes: routes,
  nodeModulesTransform: {
    type: 'none',
  },
  layout: {
    name: '1组建站后台',
    logo: false,
  },
  proxy: {
    '/api': {
      target: 'https://bjwz.bwie.com/mall4w',
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    },
  },
  title: '亚米后台',
  fastRefresh: {},
});
