import { request } from 'umi';
import { PageParams } from './pagenation';
// ---------------------------------------------------------------------
// 规格管理
export const getSpecList = (
  params: PageParams, //√
) =>
  request('/api/prod/spec/page', {
    method: 'get',
    params,
  });
// 规格管理新增
export const addSpecList = (data: any) => {
  // console.log(data,"12");
  request('/api/prod/spec', {
    method: 'post',
    data,
  });
};

// 规格 删除 /prod/spec
export const delSpecList = (id: number) => {
  //√
  request(`/api/prod/spec/${id}`, {
    method: 'delete',
  });
};
// 规格 修改接口put /prod/spec
export const editSpecList = (params: any) => {
  request('/api/prod/spec', {
    method: 'put',
    params,
  });
};
// ---------------------------------------------------------------------
// 公告
export const getNoticeList = (params: PageParams) =>
  request('/api/shop/notice/page', {
    method: 'get',
    params,
  });
// 公告获取
export const getNoticeInfo = (data: any) => {
  //√
  // console.log(data, "接口id");
  return request(`/api/shop/info/${data}`, {
    method: 'get',
    params: data,
  });
};
// 添加
export const addNoticeList = (data: any) => {
  request(`/api/shop/notice`, {
    method: 'delete',
    data,
  });
};
// 删除
export const delNoticeList = (id: string) => {
  //√
  request(`/api/shop/notice/${id}`, {
    method: 'delete',
  });
};
// 编辑
export const editNoticeList = (data: any) => {
  request(`/api/shop/notice`, {
    method: 'delete',
    data,
  });
};
// ---------------------------------------------------------------------

// 运费
export const getTransportList = (
  params: PageParams, //√
) =>
  request('/api/shop/transport/page', {
    method: 'get',
    params,
  });
// 添加 post  /shop/transport
export const addTransportList = (data: any) =>
  request('/api/shop/transport', {
    method: 'post',
    data,
  });
// 删除
export const delTransportList = (id: any) =>
  request('/api/shop/transport', {
    method: 'delete',
    data: id,
  });
// ---------------------------------------------------------------------

// 自提点部分
export const getPickAddrList = (
  params: PageParams, //√
) =>
  request('/api/shop/pickAddr/page', {
    method: 'get',
    params,
  });

// 添加post /shop/pickAddr
export const addPickAddrList = (data: any) =>
  request('/api/shop/pickAddr', {
    method: 'post',
    data,
  });
// del
export const PickAddrDel = (
  id: any, //√
) =>
  request('/api/shop/pickAddr', {
    method: 'delete',
    data: [id],
  });

// edit
export const pickAddrPut = (data: any) => {
  // console.log(data, "接口id");
  return request(`/api/shop/pickAddr`, {
    method: 'put',
    data,
  });
};
// ---------------------
//
export const getAddress = (
  params: PageParams, //√
) =>
  request('/api/shop/admin/area', {
    method: 'get',
    params,
  });
