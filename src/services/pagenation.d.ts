export interface PageParams {
  current: number;
  size: number;
  id?: number;
}

export interface DataParams {
  title: string;
  content: string;
  status: number;
  seq: number;
  hotSearchId: number;
}
