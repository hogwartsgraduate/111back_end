import { request } from 'umi';
import { PageParams } from './pagenation';

// 分组管理数据
export const getProdTagList = (params: PageParams) =>
  request('/api/prod/prodTag/page', {
    method: 'get',
    params,
  });

// 分组管理【新增】
export const getProdTagListAdd = (data) =>
  request('/api/prod/prodTag', {
    method: 'post',
    data,
  });

// 分组管理【删除】
export const getProdTagListDel = (id: number) =>
  request(`/api/prod/prodTag/${id}`, {
    method: 'delete',
  });

// 分组管理【数据回显】
export const getProdTagListInfo = (id: number) =>
  request(`/api/prod/prodTag/info/${id}`, {
    method: 'get',
  });

// 分组管理【修改确定】
export const getProdTagListEdit = (data) =>
  request('/api/prod/prodTag', {
    method: 'put',
    data,
  });

// 产品管理数据
export const getProdList = (params: PageParams) =>
  request('/api/prod/prod/page', {
    method: 'get',
    params,
  });

// 产品管理【新增页面】
// /shop/transport/list              运费设置
// /prod/spec/list                   商品规格
// /prod/spec/listSpecMaxValueId
// /prod/prodTag/listTagList         产品分组
// /prod/category/listCategory       产品分类

// 【1. 运费设置】
export const getShopTransportList = () =>
  request('/api/prod/prod/page', {
    methods: 'get',
  });

// 【2. 商品规格】
export const getProdSpecList = () =>
  request('/api/prod/spec/list', {
    methods: 'get',
  });

// 【3. 产品分组】
export const getAddProdTagList = () =>
  request('/api/prod/prodTag/listTagList', {
    methods: 'get',
  });

// 【4. 产品分类】
export const getProdCategoryList = () =>
  request('/api/prod/category/listCategory', {
    methods: 'get',
  });

// 分类管理数据
export const getCategoryList = () =>
  request('/api/prod/category/table', {
    method: 'get',
  });

// 分类管理【修改】
export const getCategoryInfo = (id: number) =>
  request(`/api/prod/category/info/${id}`, {
    method: 'get',
  });

// 分类管理【确定修改】
export const getCategoryEdit = (data) =>
  request('/api/prod/category', {
    method: 'put',
    data,
  });

// 评论管理数据 /prod/prodComm/page
export const getprodCommList = (params: PageParams) =>
  request('/api/prod/prodComm/page', {
    method: 'get',
    params,
  });
