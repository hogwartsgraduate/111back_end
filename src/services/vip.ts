import { request } from 'umi';
import { PageParams } from './pagenation';
export const getVipList = (params: PageParams) =>
  request('/api/admin/user/page', {
    method: 'get',
    params,
  });
