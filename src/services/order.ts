import { request } from 'umi';
import { PageParams } from './pagenation';
export const getOrderList = (params: PageParams) =>
  request('/api/order/order/page', {
    method: 'get',
    params,
  });
