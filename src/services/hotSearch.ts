import { request } from 'umi';
import { PageParams, DataParams } from './pagenation';
export const gethotSearch = (params: PageParams) =>
  request('/api/admin/hotSearch/page', {
    method: 'get',
    params,
  });
//热搜新增的接口
export const addHotSearch = (data: DataParams) => {
  request('/api/admin/hotSearch', {
    method: 'post',
    data,
  });
};
//热搜编辑回显接口
export const showHotSearch = (data: any) =>
  request(`/api/admin/hotSearch/info/${data}`, {
    method: 'get',
  });
//热搜编辑的接口
export const editHotSearch = (data: DataParams) => {
  request('/api/admin/hotSearch', {
    method: 'put',
    data,
  });
};

//热搜单项删除的接口
export const delHotItem = (data: any) => {
  request('/api/admin/hotSearch', {
    method: 'delete',
    data,
  });
};
