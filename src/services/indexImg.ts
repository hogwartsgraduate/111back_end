import { request } from 'umi';
import { PageParams } from './pagenation';
export const getIndexImages = (params: PageParams) =>
  request('/api/admin/indexImg/page', {
    method: 'get',
    params,
  });

//删除

export const DeleteImg = (data: any) =>
  request('/api/admin/indexImg', {
    method: 'delete',
    data,
  });
