import { request } from 'umi';

export const login = (data: any) =>
  request('/api/login', {
    method: 'post',
    data,
  });

export const getNav = (data: any) =>
  request('/api/sys/menu/nav', {
    method: 'get',
    data,
  });

//会员编辑回显接口
export const showVip = (data: any) =>
  request(`/api/admin/user/info/${data}`, {
    method: 'get',
  });
//会员编辑的接口
export const editVip = (data: any) => {
  request('/api/admin/user', {
    method: 'put',
    data,
  });
};
