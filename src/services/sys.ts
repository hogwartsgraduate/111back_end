import { request } from 'umi';

//系统管理->系统日志  /sys/log/page?t=1664273667463&current=1&size=10
export const getSysLog = (params: any) =>
  request('/api/sys/log/page', {
    method: 'get',
    params,
  });
//系统管理->参数管理  /sys/config/page?t=1664281523245&current=1&size=10
export const getSysConfig = (params: any) =>
  request('/api/sys/config/page', {
    method: 'get',
    params,
  });
//系统管理->管理员列表   /sys/user/page?t=1664283733481&current=1&size=10
export const getSysUser = (params: any) =>
  request('/api/sys/user/page', {
    method: 'get',
    params,
  });

//系统管理->角色管理    /sys/role/page?t=1664284735877&current=1&size=10
export const getSysRole = (params: any) =>
  request('/api/sys/role/page', {
    method: 'get',
    params,
  });

//系统管理->定时任务   /sys/schedule/page?t=1664321642342&page=1&limit=10&beanName=
export const getSysSchedule = (params: any) =>
  request('/api/sys/schedule/page', {
    method: 'get',
    params,
  });

//系统管理-> 参数管理(新增)   /sys/config
export const addSysConfig = (data: any) =>
  request('/api/sys/config', {
    method: 'post',
    data,
  });

//系统管理-> 参数管理(删除) /sys/config
export const delSysSchedule = (data: any) =>
  request('/api/sys/config', {
    method: 'delete',
    data,
  });

//系统管理-> 管理员列表(新增)   /sys/user
export const addSysUser = (data: any) =>
  request('/api/sys/user', {
    method: 'post',
    data,
  });
//系统管理-> 管理员列表(删除) /sys/user
export const delSysUser = (data: any) =>
  request('/api/sys/user', {
    method: 'delete',
    data,
  });

//系统管理-> 参数管理(回显)  /sys/config/info/8?t=1664421770646
export const getSysConfigBack = (id: any) =>
  request(`/api/sys/config/info/${id}`, {
    method: 'get',
  });

//系统管理-> 参数管理(编辑)  /sys/config
export const getSysConfigEdit = (data: any) =>
  request(`/api/sys/config`, {
    method: 'put',
    data
  });

//系统管理->管理员列表(回显)  /sys/user/info/1?t=1664436417779
export const getSysUserBack = (id: any) =>
  request(`/api/sys/user/info/${id}`, {
    method: 'get',
  });

//系统管理-> 管理员列表(编辑)  /sys/user
export const getSysEdit = (data: any) =>
  request(`/api/sys/user`, {
    method: 'put',
    data
  });

//系统管理->地址管理   /admin/area/list?t=1664502772930&current=1&size=10
export const getSysArea = (params: any) =>
  request('/api/admin/area/list', {
    method: 'get',
    params,
  });

//系统管理->地址管理（删除） /admin/area/659006000021
export const delSysArea = (id: any) =>
  request(`/api/admin/area/${id}`, {
    method: 'delete',
  });

//系统管理->菜单管理  /sys/menu/table?t=1664936911313
export const getSysMenu = ( )=>
  request('/api/sys/menu/table', {
    method: 'get',
  });

//系统管理->地址管理（新增） /admin/area
export const addSysAtea = (data: any) =>
  request('/api/admin/area', {
    method: 'post',
    data,
  });

//系统管理->地址管理（编辑回显）  /admin/area/info/659006000063?t=1664969569756
export const getSysAteaItems = (id:any) =>
request(`/api/admin/area/info/${id}`, {
  method: 'get'
});

//系统管理->地址管理（编辑） /admin/area
export const editSysAtea = (data: any) =>
request('/api/admin/area', {
  method: 'put',
  data,
});

//系统管理->角色管理（删除）  /sys/role
export const delSysRole = (data: any) =>
  request(`/api/sys/role`, {
    method: 'delete',
    data
  });

//系统管理->角色管理（新建，编辑弹框的多选）  /sys/menu/table
export const getSysRoleTan= () =>
  request(`/api/sys/menu/table`, {
    method: 'get',
  });

//系统管理->角色管理（新建）  /sys/role
export const addSysRole= (data:any) =>
  request(`/api/sys/role`, {
    method: 'post',
    data
  });

//系统管理->角色管理（编辑回显）  /sys/role/info/31?t=1665045213239
export const getSysRoleEdit = (id:any) =>
request(`/api/sys/role/info/${id}`, {
  method: 'get'
});

//系统管理-> 角色管理（编辑）  /sys/role
export const editSysRole = (data: any) =>
request('/api/sys/role', {
  method: 'put',
  data,
});