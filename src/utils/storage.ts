interface LocalType {
  type?: 'local' | 'session';
  name: string;
}
class LocalStore {
  storage: any;
  name: string;
  constructor({ type = 'local', name }: LocalType) {
    this.storage = window[type + 'Storage'];
    this.name = name;
  }
  get() {
    try {
      return JSON.parse(this.storage.getItem(this.name));
    } catch (err) {
      return this.storage.getItem(this.name);
    }
  }
  set(value) {
    this.storage.setItem(this.name, JSON.stringify(value));
  }
  remove() {
    this.storage.removeItem(this.name);
  }
}
export default LocalStore;
