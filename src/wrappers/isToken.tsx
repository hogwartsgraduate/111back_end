import { Redirect } from 'umi';
import { userLocalStore } from '@/pages/login';

export default (props) => {
  try {
    const userInfo = userLocalStore.get();
    const expires_in = userInfo?.expires_in;
    if (expires_in > Date.now()) {
      return props.children;
    }
    return <Redirect to="/login" />;
  } catch (err) {
    return <Redirect to="/login" />;
  }
};
