import { RequestConfig, history } from 'umi';
import { userLocalStore } from '@/pages/login';
import { message } from 'antd';
import { getNav } from '@/services/user';
import LocalStore from '@/utils/storage';

const menuListStorage = new LocalStore({
  type: 'session',
  name: 'menuList',
});
const authoritiesStorage = new LocalStore({
  type: 'session',
  name: 'authorities',
});
const whiteList = ['/login'];
const codeMaps = {
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};
export const request: RequestConfig = {
  //覆盖request的默认配置
  errorConfig: {
    adaptor: (resData) => {
      //拦截器的返回值
      return resData;
    },
  },
  requestInterceptors: [
    async (url, options) => {
      const t = Date.now();
      options.method === 'post'
        ? (options.data.t = t)
        : options.method === 'delete'
        ? ''
        : options.method === 'put'
        ? ''
        : typeof options.params === 'object' && (options.params.t = t);
      const headers = {
        ...options.headers,
      };
      if (whiteList.findIndex((val) => url.includes(val)) < 0) {
        headers.Authorization =
          userLocalStore.get()?.tokenType + userLocalStore.get()?.token;
      }
      return {
        url,
        options: {
          ...options,
          headers,
        },
      };
    },
  ],
  responseInterceptors: [
    async (response) => {
      if (response.ok) {
        return response;
      }
      if (response.status === 401) {
        message.error('您的登录状态过期请重新登录', 1, () => {
          history.replace('/login');
        });
        return {
          ...response,
        };
      }
      try {
        const res = await response.text();
        return {
          ...response,
          errorMessage:
            res || response.statusText || codeMaps[`${response.status}`],
          showType: 1,
          success: false,
        };
      } catch (err) {
        return {
          ...response,
          errorMessage: '接口格式错误',
          showType: 2,
          success: false,
        };
      }
    },
  ],
};

let extraRoutes;

const init = async () => {
  const { menuList, authorities } = await getNav();
  menuListStorage.set(menuList);
  authoritiesStorage.set(authorities);
};
// const formatMenuList = (arr, parentArr) => {
//     Array.isArray(arr) && arr.forEach(item => {
//         const o = {
//             name: item.name,
//         }
//         if (item.url) {
//             o.path = '/' + item.url;
//             o.component = require(`@/pages${o.path}/index.tsx`).default;
//         }
//         if (item.list && Array.isArray(item.list)) {
//             o.routes = [];
//             formatMenuList(item.list, o.routes)
//         }
//         parentArr.push(o);
//     })
// }
const formatMenuList = (arr, parentArr, rootPath) => {
  Array.isArray(arr) &&
    arr.forEach((item) => {
      const o = {
        name: item.name,
        path: item.parentId === 0 ? '/' + item.list[0].url.split('/')[0] : '',
      };
      try {
        if (item.url) {
          o.id = item.url;
          o.exact = true;
          o.path = `/${item.url}`.startsWith(rootPath)
            ? `/${item.url}`
            : `${rootPath}/${item.url}`;
          o.component = require(`@/pages${o.path}/index.tsx`).default;
        }
      } catch (error) {
        o.component =
          error.code === 'MODULE_NOT_FOUND' &&
          require(`@/pages/404/index404.tsx`).default;
      }
      if (item.list && Array.isArray(item.list)) {
        o.routes = [];
        formatMenuList(item.list, o.routes, o.path);
      }
      parentArr.push(o);
    });
};

export function patchRoutes({ routes }) {
  // 合并路由
  const menulist = menuListStorage.get();
  formatMenuList(menulist, routes[0].routes, '/');
  // menulist.forEach(item => {
  //   routes[0].routes.push(({
  //     name: item.name,
  //     exact:true,
  //     path:'/foo',
  //     routes: []
  //   }))
  // })
  console.log('patchRoutes---', menulist, routes[0].routes);
}
const notAddRoutePagesList = ['/login'];
export async function render(oldRender) {
  //合并路由
  console.log('render---');
  const { pathname } = history.location;
  if (notAddRoutePagesList.includes(pathname)) {
    oldRender();
    return;
  }
  await init(); //获取权限
  oldRender();
}
