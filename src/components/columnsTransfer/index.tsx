import { Modal, Transfer } from 'antd';
import { ProColumns, ProFormSegmented } from '@ant-design/pro-components';
import { FC, useEffect, useMemo, useState } from 'react'; //FC函数式组件

// 定义泛型
interface Iprops {
  show: boolean;
  columns: ProColumns<{ notColumnShow: boolean }>[]; //每个组件的扩展不一样，
  modalConfig?: any; //modalConfig值是一个对象，对象的类型取决于Modal参数的类型
  transferConfig?: any;
  onShowChange?: Function;
  onCancel?: Function;
}

// 穿梭框数据
// interface Itransfer {
//     title: string,
//     key: string,
//     isShow: boolean,
// }

const Com: FC<Iprops> = (props: Iprops) => {
  const [dataSource, setDataSource] = useState<any>([]); //dataSource为当前组件属性
  // 1. 除了操作那一列，剩下的显示在穿梭框中的数据    没有办法直接修改props
  useEffect(() => {
    setDataSource(
      props.columns
        .filter((val) => !val.notColumnShow)
        .map((item) => ({
          //筛选出符合notColumnShow为false条件的列
          title: item.title,
          key: item.dataIndex,
          isShow: true, //默认【显示】，显示在右侧列表中
        })),
    );
  }, []);

  // 2. 一打开弹框，数据就显示在右侧， targetKeys，控制右侧显示那一栏，它由isShow控制
  const targetKeys = useMemo(() => {
    return dataSource.filter((val) => val.isShow).map((item) => item.key); //targetKeys	显示在右侧框数据的 key 集合
  }, [dataSource]);

  // 3.监听事件 点击左右按钮时触发， 只改变targetKeys， dataSource数据也跟着改变   修改dataSource的isShow属性
  const handleChange = (targetKeys, direction: string, moveKeys) => {
    console.log(targetKeys, direction, moveKeys, '--------------------------');

    // moveKeys 点击选中要移动的key
    // 修改dataSource
    dataSource.map((item) => {
      if (moveKeys.includes(item.key)) {
        item.isShow = direction === 'right';
      }
    });
    setDataSource([...dataSource]);

    // 4.驱动页面视图改变
    props.onShowChange && props.onShowChange(dataSource);
  };

  return (
    <Modal footer={null} open={props.show} {...props.modalConfig}>
      <Transfer
        dataSource={dataSource} //dataSource 整体原数据，对其进行处理，让操作那一列不显示
        titles={['隐藏', '显示']}
        render={(item) => item.title}
        onChange={handleChange}
        targetKeys={targetKeys}
      />
    </Modal>
  );
};

export default Com;
