import { useEffect, useState } from 'react';
import styles from './index.less';
import { getSysSchedule } from '@/services/sys';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { Button, Tag } from 'antd';
import className from 'classnames';
import { ToolOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
const objStatus = {
  1: '暂停',
  0: '正常',
};
const columns = [
  {
    title: 'ID',
    width: 100,
    align: 'center',
    search: false,
    dataIndex: 'jobId',
  },
  {
    title: 'bean名称',
    dataIndex: 'beanName',
    align: 'center',
    width: 160,
  },
  {
    title: '方法名称',
    dataIndex: 'methodName',
    align: 'center',
    search: false,
    width: 160,
  },
  {
    title: '参数',
    dataIndex: 'params',
    align: 'center',
    search: false,
    width: 160,
  },
  {
    title: 'cron表达式',
    dataIndex: 'cronExpression',
    align: 'center',
    search: false,
    width: 160,
  },
  {
    title: '备注',
    dataIndex: 'remark',
    align: 'center',
    search: false,
    width: 160,
  },
  {
    title: '状态',
    dataIndex: 'status',
    align: 'center',
    search: false,
    width: 160,
    render: (text, records, index) => {
      if (records.status == 1) {
        return <Tag color="red">{objStatus[records.status]}</Tag>;
      }
      return <Tag color="blue">{objStatus[records.status]}</Tag>;
    },
  },
  {
    title: '操作',
    search: false,
    align: 'center',
    width: 180,
    render: () => [
      <a className={styles.aStyle}>修改</a>,
      <a className={styles.aStyle}>删除</a>,
      <a className={styles.aStyle}>暂停</a>,
      <a className={styles.aStyle}>恢复</a>,
      <a className={styles.aStyle}>立即执行</a>,
    ],
  },
];

const scheduleindex = () => {
  const request = async (arg) => {
    arg.page = arg.current;
    arg.limit = arg.pageSize;
    delete arg.pageSize;
    delete arg.current;
    const res = await getSysSchedule({
      ...arg,
    });
    console.log(res);

    return {
      data: res.records,
      success: true,
      total: res.total,
    };
  };

  return (
    <div>
      <ProTable
        toolBarRender={false}
        columns={columns}
        request={request}
        rowKey="jobId"
        bordered={true}
        pagination={{
          pageSize: 10,
        }}
        rowSelection={{
          // selectedRowKeys,
          // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
          // 注释该行则默认不显示下拉选项
          // selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
          defaultSelectedRowKeys: [1],
        }}
      />
    </div>
  );
};

export default scheduleindex;
