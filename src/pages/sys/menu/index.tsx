import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import { Button, Tag, Modal } from 'antd';
import className from 'classnames';
import {
  ToolOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  PlusOutlined
} from '@ant-design/icons';
import { getSysMenu } from '@/services/sys';

//处理数据格式
const formatAreaList = (data, parentId: number) => {
  return data.filter(val => val.parentId === parentId).map(item => {
    return {
      ...item,
      children: formatAreaList(data, item.menuId)
    }
  })
}

const menuindex = () => {
  const [titles, settitles] = useState('')//表格标题
  const [ItemsId, setItemsId] = useState('')//编辑id
  const editForm = useRef(null)
  const objStatus = {
    0: '目录',
    1: '菜单',
    2: '按钮',
  };

  const columns = [
    {
      title: '名称',
      search: false,
      width: 160,
      align: 'center',
      dataIndex: 'name',
    },
    {
      title: '图标',
      dataIndex: 'icon',
      align: 'center',
      search: false,
      width: 160,
      // render: (text, records, index) =>{
      //   return <records.icon/>
      // }
    },
    {
      title: '类型',
      dataIndex: 'type',
      align: 'center',
      search: false,
      width: 160,
      render: (text, records, index) => {
        if(records.type==0){
          return <Tag color="green">{objStatus[records.type]}</Tag>;
        }else if(records.type==1){
          return <Tag color="blue">{objStatus[records.type]}</Tag>;
        }else{
          return <Tag color="#ccc">{objStatus[records.type]}</Tag>;
        }
      },
    },
    {
      title: '排序号',
      dataIndex: 'orderNum',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '菜单URL',
      dataIndex: 'url',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '授权标识',
      dataIndex: 'perms',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '操作',
      align: 'center',
      search: false,
      notColumnShow: true, //在穿梭框的数据中不展示操作那一列
      width: 230,
      render: (text, record, index, action) => [
        <Button key="link" type="link" className={styles.btnML}
        onClick={() => {
          getItems(record);
        }}>
          修改
        </Button>,
        <Button
          key="link2"
          type="link"
          onClick={() => delItems(record)}
        >
          删除
        </Button>,
      ],
    },
  ];

    //编辑回显
    const getItems = async (record) => {
      settitles('编辑')
      setIsModalOpen(true);
      // setItemsId(record.userId);
      // const res = await getSysUserBack(record.userId);
      // res.status = objStatus[res.status]
      // editForm.current.setFieldsValue(res)//回显赋值areaList
    }
  
    const request = async (arg) => {
      const res = await getSysMenu()
      return {
        data: formatAreaList(res,0),
        success: true,
      };
    };
    const ref = useRef(null);
    //弹框
    // const [isModalOpen, setIsModalOpen] = useState(false);
    // const handleOk = () => {
    //   setIsModalOpen(false);
    // };
    // const handleCancel = () => {
    //   setIsModalOpen(false);
    // };
    //单项删除
    const { confirm } = Modal;
    const delItems = (record) => {
      console.log(record);
      // confirm({
      //   title: `确定对id=${record.id}进行删除吗`,
      //   icon: <ExclamationCircleOutlined />,
      //   okText: 'Yes',
      //   okType: 'danger',
      //   cancelText: 'No',
      //   async onOk() {
      //     const res = await delSysUser([record.userId]);
      //     ref.current.reload();
      //   },
      //   onCancel() {
      //     console.log('Cancel');
      //   },
      // });
    };
  
    //弹框里的表单
    const onFinish = async (values: any) => {
      console.log('Success:', values);
      if (titles == '新增') {
        // delete values.ConfirmPassword;
        // const res = await addSysUser({
        //   ...values,
        //   t: Date.now(),
        // });
      } else {
        // const res = await getSysEdit({ userId: ItemsId, ...values });
      }
      ref.current.reload();
    };
    // const onFinishFailed = (errorInfo: any) => {
    //   console.log('Failed:', errorInfo);
    // };
    // const addMoreList = () => {
    //   settitles('新增')
    //   setIsModalOpen(true);
    // };
  return <div>
    <ProTable
      columns={columns}
      request={request}
      rowKey="menuId"
      actionRef={ref}
      bordered={true}
      pagination={false}
      search={false}
      toolbar={{
        subTitle: [
          <Button type="primary" key="show">
            <PlusOutlined />
            新增
          </Button>,
        ],
        settings: []
      }}
    />
  </div>;
};

export default menuindex;
