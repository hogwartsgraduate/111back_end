import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import { getSysUser, addSysUser, delSysUser, getSysUserBack,getSysEdit } from '@/services/sys';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { Button, Input, Modal, Form, Checkbox, Col, Row, Radio } from 'antd';
import className from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';
import {
  ToolOutlined,
  DeleteOutlined,
  PlusOutlined,
  AppstoreOutlined,
  SearchOutlined,
  RedoOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';

const sysUserindex = () => {
  const [flag, setflag] = useState(true)
  const [titles, settitles] = useState('')//表格标题
  const [ItemsId, setItemsId] = useState('')//编辑id
  const editForm = useRef(null)
  const objStatus = {
    1: '正常',
    0: '禁用',
  };
  const columns = [
    {
      title: '用户名',
      width: 160,
      align: 'center',
      dataIndex: 'username',
      // renderFormItem(item,options,form){
      //   const changesInput = (value) =>{
      //     console.log(value);
      //     // form.setFieldValue('username',value)
      //   }
      //   return <Input onChange={changesInput}/>
      // }
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '手机号',
      dataIndex: 'mobile',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      align: 'center',
      search: false,
      width: 160,
    },
    {
      title: '状态',
      dataIndex: 'status', // 1正常  0禁用
      align: 'center',
      search: false,
      width: 160,
      render: (text, records, index) => {
        return <p>{objStatus[records.status]}</p>;
      },
    },
    {
      title: '操作',
      align: 'center',
      search: false,
      notColumnShow: true, //在穿梭框的数据中不展示操作那一列
      width: 230,
      render: (text, record, index, action) => [
        <Button key="link" type="primary" className={styles.btnML}
        onClick={() => {
          getItems(record);
        }}>
          <ToolOutlined />
          编辑
        </Button>,
        <Button
          key="link2"
          type="primary"
          danger
          onClick={() => delItems(record)}
        >
          <DeleteOutlined />
          删除
        </Button>,
      ],
    },
  ];

  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  // 更改页面视图
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };

  //编辑回显
  const getItems = async (record) => {
    settitles('编辑')
    setIsModalOpen(true);
    setItemsId(record.userId);
    const res = await getSysUserBack(record.userId);
    res.status = objStatus[res.status]
    editForm.current.setFieldsValue(res)//回显赋值
  }

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const res = await getSysUser({
      ...arg,
    });
    return {
      data: res.records,
      success: true,
      total: res.total,
    };
  };
  const ref = useRef(null);
  //弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //单项删除
  const { confirm } = Modal;
  const delItems = (record) => {
    console.log(record);
    confirm({
      title: `确定对id=${record.id}进行删除吗`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        const res = await delSysUser([record.userId]);
        ref.current.reload();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  //弹框里的表单
  const onFinish = async (values: any) => {
    console.log('Success:', values);
    if (titles == '新增') {
      delete values.ConfirmPassword;
      const res = await addSysUser({
        ...values,
        t: Date.now(),
      });
    } else {
      const res = await getSysEdit({ userId: ItemsId, ...values });
    }

    ref.current.reload();
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  const addMoreList = () => {
    settitles('新增')
    setIsModalOpen(true);
  };
   //选中的函数
   const [selectedRowKeys,setselectedRowKeys] = useState([])
   const onSelectChange = (newSelectChange)=>{
     setselectedRowKeys(newSelectChange)
     console.log(newSelectChange);
   }
   const rowSelection = {
     selectedRowKeys,
     onChange: onSelectChange,
   }
   //批量删除
   const delItemsAll = (selectedRowKeys) => {
    confirm({
      title: `确定对id=${selectedRowKeys}进行删除吗`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        const res = await delSysUser(selectedRowKeys);
        ref.current.reload();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  return (
    <div>
      <Modal title={titles} open={isModalOpen} footer={null}>
        <Form
          name="basic"
          ref={editForm}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: '请输入用户名!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码!' }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="确认密码"
            dependencies={['password']}
            name="ConfirmPassword"
            rules={[
              {
                required: true,
                message: '请确认密码!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('两次密码不一样!'));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item label="邮箱" name="email" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item label="手机号" name="mobile" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="roleIdList" label="角色">
            <Checkbox.Group>
              {/* 运营  7     管理员  1     VIP  8 */}
              <Row>
                <Col span={10}>
                  <Checkbox value="1" style={{ lineHeight: '32px' }}>
                    管理员
                  </Checkbox>
                </Col>
                <Col span={8}>
                  <Checkbox value="7" style={{ lineHeight: '32px' }}>
                    运营
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="8" style={{ lineHeight: '32px' }}>
                    VIP
                  </Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>
          </Form.Item>
          <Form.Item name="status" label="状态">
            <Radio.Group>
              <Radio value="1">正常</Radio>
              <Radio value="0">禁用</Radio>
            </Radio.Group>
          </Form.Item>

          <div className={styles.subTit}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" onClick={handleOk}>
                提交
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button onClick={handleCancel}>取消</Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
      <ProTable
        columns={columnsConfig}
        request={request}
        rowKey="userId"
        actionRef={ref}
        search={flag}
        bordered={true}
        pagination={{
          pageSize: 10,
        }}
        form={{
          resetText: '清空',
          searchText: '搜索',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
        rowSelection={rowSelection}
        toolbar={{
          subTitle: [
            <Button type="primary" key="show" onClick={addMoreList}>
              <PlusOutlined />
              新增
            </Button>,
            <Button type="primary" key="primary" danger onClick={()=>delItemsAll(selectedRowKeys)} >
              <DeleteOutlined />
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
      />
      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};

export default sysUserindex;
