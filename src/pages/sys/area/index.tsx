import { Input, Tree,Button,Modal,Form ,Cascader } from 'antd';
import  { useEffect, useRef, useState } from 'react';
import styles from './index.less';
import { getSysArea,delSysArea,addSysAtea,editSysAtea,getSysAteaItems} from '@/services/sys'
import {ExclamationCircleOutlined} from '@ant-design/icons';
interface AreaItem {
  areaId:number;
  areaName:string;
  areas:null;
  level:number;
  parentId:number;
}
interface TreeItem {
  key:number;
  title:string;
  children:TreeItem[]
}
//处理数据格式
const formatAreaList = (data:AreaItem[],parentId:number):TreeItem[]=>{
  return data.filter(val =>val.parentId === parentId).map(item=>{
    return{
      key:item.areaId,
      title:item.areaName,
      children:formatAreaList(data,item.areaId)
    }
  })
}

const areaindex = () => {
  let {Search} = Input
  const [areaList,setAreaList] = useState<TreeItem[]>([])
  const [areaSource,setAreaSource] = useState([])
  const init = async () => {
    const res = await getSysArea({
      current: 1,
      size: 10
    })
    setAreaSource(res)
    setAreaList(formatAreaList(res,0))
  }
  //单项删除
  const { confirm } = Modal;
  const delItems = (nodeData) => {
    console.log(nodeData);
    confirm({
      title: `确定对id=${nodeData.key}进行删除吗`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        await delSysArea([nodeData.key]);
        init()
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };
  const titleRender = (nodeData:TreeItem)=>{
    return <div className={styles.TreeItems}>
      <p>{nodeData.title}</p>
      <div>
      <Button type='link' onClick={()=>getItems(nodeData)}>编辑</Button>
      <Button type='link' onClick={()=>delItems(nodeData)}>删除</Button>
      </div>
    </div>
  }
  //模糊搜索
  const findParentIds = (value:string,sourceArr:AreaItem[]) =>{
    const res:AreaItem[] = []
    const curValArr = sourceArr.filter(item=>item.areaName?.includes(value))// 从最基础的数组中找到areaName包含value的值
    const findParent= (item)=>{//查找父级
      if(item.parentId !== 0){
        const parent = sourceArr.find(val=>val.areaId === item.parentId)
        parent && findParent(parent)
      }
      res.findIndex(val => val.areaId === item.areaId) < 0 && res.push(item); // 判断在数组中出现过
    }
    curValArr.map(findParent)
    return res
  }
  const onChange =(e: React.ChangeEvent<HTMLInputElement>) =>{
    const { value } = e.target;
    if(value){
      const values = findParentIds(value,areaSource)
       setAreaList(formatAreaList(values,0))//查找元素
    }else{
      setAreaList(formatAreaList(areaSource,0))//刷新页面
    }
  }

  const [titles,settitles] = useState('')//表格标题
  const [Items,setItems] = useState('')//编辑id
  
  const editForm = useRef(null)
   //弹框
   const [isModalOpen, setIsModalOpen] = useState(false);
   const handleOk = () => {
     setIsModalOpen(false);
   };
   const handleCancel = () => {
     setIsModalOpen(false);
   };
    //编辑回显
  const getItems =async(nodeData)=>{
    settitles('编辑')
    setIsModalOpen(true);
    setItems(nodeData);
    const res  = await getSysAteaItems(nodeData.key);
    const resList = findParentIds(res.areaName,areaSource)
    const arr =[]
    resList.map(item=>{
      arr.push(item.areaName)
      return arr 
    })
    const str =arr.join(",").replace(/\,/g,"/")
    console.log(str);
    editForm.current.setFieldsValue({
      areaName:res.areaName,
      Area:str
    })//回显赋值
  }
  //弹框里的表单
  const onFinish = async (values: any) => {
    console.log('Success:', values);
    console.log('Items:', Items);
    if(titles == '新增'){
      const res = await addSysAtea({ 
        areaName:values.areaName,
        areaId:0,
        level:null,
        parentId:values.Area[values.Area.length - 1],
        t:Date.now()
      });
    }else{
      const res = await editSysAtea({
        areaName:values.areaName,
        areaId:Items.key,
        level:values.level,
        parentId:values.Area[values.Area.length - 1],
        t:Date.now()
       });
    }
    init()
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  const addMoreList = () => {
    setIsModalOpen(true);
    settitles('新增')
  };
  useEffect(() => {
    init()
  }, [])
  return (
    <div>
      <div  className={styles.TreeTitle}>
      <Search style={{ marginBottom: 8 }} placeholder="Search" onChange={onChange}  className={styles.TreeInput} />
      <Button type='primary' onClick={()=>addMoreList()}>新增</Button>
      </div>
      <Tree
        titleRender={titleRender}
        treeData={areaList}
      />
       <Modal title={titles} open={isModalOpen} footer={null}>
        <Form
          ref={editForm}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          className={styles.Forms}
        >
          <Form.Item
            label="地区名称"
            name="areaName"
            rules={[{ required: true, message: '请输入地区名称!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
           label="上级地区"
           name="Area"
          >
          <Cascader fieldNames={{ label: 'title', value: 'key' }}
          options={areaList} placeholder="Please select" />
          {/*   */}
          </Form.Item>
    
          <div className={styles.subTit}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" onClick={handleOk}>
                提交
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button onClick={handleCancel}>取消</Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </div>
  );
}

export default areaindex;
