import { useRef, useState } from 'react';
import styles from './index.less';
import { getSysLog } from '@/services/sys';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { Button, Table } from 'antd';
import ColumnsTransfer from '@/components/columnsTransfer';
import {
  ToolOutlined,
  DeleteOutlined,
  PlusOutlined,
  AppstoreOutlined,
  SearchOutlined,
  RedoOutlined,
} from '@ant-design/icons';
const columns = [
  {
    title: '用户名',
    width: 160,
    align: 'center',
    dataIndex: 'username',
  },
  {
    title: '用户操作',
    dataIndex: 'operation',
    ellipsis: true,
    align: 'center',
    width: 160,
  },
  {
    title: '请求方法',
    width: 160,
    dataIndex: 'method',
    align: 'center',
    search: false,
  },
  {
    title: '请求参数',
    width: 160,
    dataIndex: 'params',
    align: 'center',
    search: false,
  },
  {
    title: '执行时长（毫秒）',
    width: 160,
    dataIndex: 'time',
    align: 'center',
    search: false,
  },
  {
    title: 'IP地址',
    dataIndex: 'ip',
    width: 160,
    align: 'center',
    search: false,
  },
  {
    title: '创建时间',
    width: 180,
    dataIndex: 'createDate',
    align: 'center',
    search: false,
  },
];

const sysLogindex = () => {
  const ref = useRef(null);
  const [flag,setflag] = useState(true)
  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  // 更改页面视图
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };


  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const res = await getSysLog({
      ...arg,
    });
    return {
      data: res.records,
      success: true,
      total: res.total,
    };
  };

  return (
    <div>
      <ProTable
        actionRef={ref}
        search={flag}
        columns={columnsConfig}
        request={request}
        rowKey="id"
        bordered={true}
        pagination={{
          pageSize: 10,
        }}
        form={{
          resetText: '清空',
          searchText: '搜索',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
        rowSelection={{
          // selectedRowKeys,
          // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
          // 注释该行则默认不显示下拉选项
          selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
          defaultSelectedRowKeys: [1],
        }}
        toolbar={{
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
      />
      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};

export default sysLogindex;
