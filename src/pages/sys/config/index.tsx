import { useEffect, useRef, useState } from 'react';
import styles from './index.less';
import {
  getSysConfig,
  addSysConfig,
  delSysSchedule,
  getSysConfigBack,
  getSysConfigEdit,
} from '@/services/sys';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { Button, Modal, Checkbox, Form, Input } from 'antd';
import className from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';
import {
  ToolOutlined,
  DeleteOutlined,
  PlusOutlined,
  AppstoreOutlined,
  SearchOutlined,
  RedoOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';

const sysconfigindex = () => {
  const [flag, setflag] = useState(true);
  const [titles, settitles] = useState(''); //表格标题
  const [ItemsId, setItemsId] = useState(''); //编辑id
  const editForm = useRef(null);
  const columns = [
    {
      title: '参数名',
      width: 230,
      align: 'center',
      dataIndex: 'paramKey',
    },
    {
      title: '参数值',
      dataIndex: 'paramValue',
      align: 'center',
      search: false,
      width: 230,
    },
    {
      title: '备注',
      width: 230,
      dataIndex: 'remark',
      align: 'center',
      search: false,
    },
    {
      title: '操作',
      align: 'center',
      search: false,
      notColumnShow: true, //在穿梭框的数据中不展示操作那一列
      width: 230,
      render: (text, record, index, action) => [
        <Button
          key="link"
          type="primary"
          className={styles.btnML}
          onClick={() => {
            getItems(record);
          }}
        >
          <ToolOutlined />
          编辑
        </Button>,
        <Button
          key="link2"
          type="primary"
          danger
          onClick={() => {
            delItems(record);
          }}
        >
          <DeleteOutlined />
          删除
        </Button>,
      ],
    },
  ];

  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  // 更改页面视图
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };

  const ref = useRef(null);
  //弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //单项删除
  const { confirm } = Modal;
  const delItems = (record) => {
    console.log(record);
    confirm({
      title: `确定对id=${record.id}进行删除吗`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        const res = await delSysSchedule([record.id]);
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  //编辑回显
  const getItems = async (record) => {
    settitles('编辑');
    setIsModalOpen(true);
    setItemsId(record.id);
    const res = await getSysConfigBack(record.id);
    editForm.current.setFieldsValue(res); //回显赋值
  };

  //弹框里的表单
  const onFinish = async (values: any) => {
    console.log('Success:', values);
    if (titles == '新增') {
      const res = await addSysConfig({ ...values });
    } else {
      const res = await getSysConfigEdit({ id: ItemsId, ...values });
    }
    ref.current.reload();
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  //选中的函数
  const [selectedRowKeys, setselectedRowKeys] = useState([]);
  const onSelectChange = (newSelectChange) => {
    setselectedRowKeys(newSelectChange);
    console.log(newSelectChange);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  //多项删除
  const delItemsAll = (selectedRowKeys) => {
    console.log(selectedRowKeys);
    confirm({
      title: `确定对id=${selectedRowKeys}进行删除吗`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        // const res = await delSysSchedule(selectedRowKeys);
        ref.current.reload();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  //表格的request函数
  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const res = await getSysConfig({
      ...arg,
    });

    return {
      data: res.records,
      success: true,
      total: res.total,
    };
  };
  const addMoreList = () => {
    setIsModalOpen(true);
    settitles('新增');
  };

  return (
    <div>
      <Modal title={titles} open={isModalOpen} footer={null}>
        <Form
          ref={editForm}
          className={styles.Forms}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="参数名"
            name="paramKey"
            rules={[{ required: true, message: '请输入参数名!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="参数值"
            name="paramValue"
            rules={[{ required: true, message: '请输入参数值!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="备注" name="remark">
            <Input />
          </Form.Item>
          <div className={styles.subTit}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" onClick={handleOk}>
                提交
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button onClick={handleCancel}>取消</Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
      <ProTable
        actionRef={ref}
        columns={columnsConfig}
        search={flag}
        request={request}
        rowKey="id"
        bordered={true}
        pagination={{
          pageSize: 10,
        }}
        rowSelection={rowSelection}
        form={{
          resetText: '清空',
          searchText: '搜索',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
        toolbar={{
          subTitle: [
            <Button type="primary" key="show" onClick={() => addMoreList()}>
              <PlusOutlined />
              新增
            </Button>,
            <Button
              type="primary"
              key="primary"
              danger
              onClick={() => delItemsAll(selectedRowKeys)}
            >
              <DeleteOutlined />
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
      />
      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};
export default sysconfigindex;
