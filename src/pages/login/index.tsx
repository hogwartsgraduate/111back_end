import styles from './index.less';
import {
  AlipayCircleOutlined,
  LockOutlined,
  MobileOutlined,
  TaobaoCircleOutlined,
  UserOutlined,
  WeiboCircleOutlined,
} from '@ant-design/icons';
import { LoginForm, ProFormText } from '@ant-design/pro-components';
import logo111 from '../../static/logo.png';
import { v4 as uuidv4 } from 'uuid';
import { useEffect, useMemo, useState } from 'react';
import className from 'classnames';
import { login, getNav } from '@/services/user';
import { history } from 'umi';
import LocalStore from '@/utils/storage';

export const userLocalStore = new LocalStore({
  type: 'local',
  name: 'userInfo',
});

const LoginPage = () => {
  const [uuid, setUuid] = useState(() => uuidv4());
  const [codeImageLoading, setcodeImageLoading] = useState(false);
  const ImageUrl = useMemo(
    () => `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${uuid}`,
    [uuid],
  );
  const clickChangeImage = async () => {
    setcodeImageLoading(true);
    setUuid(uuidv4());
  };
  const submitAllThings = async (values) => {
    values = {
      ...values,
      sessionUUID: uuid,
    };
    const res = await login(values);
    userLocalStore.set({
      token: res.access_token,
      tokenType: res.token_type,
      expires_in: Date.now() + res.expires_in * 1000,
    });
    window.location.href = '/';
  };
  return (
    <div className={styles.loginBody}>
      <div className={styles.loginPages}>
        <LoginForm
          title="亚米登录"
          subTitle="1912-1组搭建，管理商品内部数据使用"
          logo={logo111}
          onFinish={submitAllThings}
        >
          <ProFormText
            name="principal"
            fieldProps={{
              size: 'large',
              prefix: <UserOutlined className={'prefixIcon'} />,
            }}
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
            placeholder={'账号'}
          />
          <ProFormText.Password name="credentials" placeholder={'密码'} />
          <div id={styles.captchaStyle}>
            <ProFormText name="imageCode" placeholder={'验证码'} />
            <div
              className={className(
                {
                  [styles.loading]: codeImageLoading,
                },
                styles.codeImage,
              )}
              onLoad={() => {
                setcodeImageLoading(false);
              }}
              onError={() => {
                setcodeImageLoading(true);
              }}
            >
              <img
                src={ImageUrl}
                onClick={() => {
                  clickChangeImage();
                }}
              />
            </div>
          </div>
        </LoginForm>
      </div>
    </div>
  );
};
export default LoginPage;
