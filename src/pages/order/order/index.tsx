import { useState, useEffect } from 'react';
import './style.less';
import {
  ProForm,
  ProFormText,
  ProFormDateRangePicker,
  ProFormSelect,
} from '@ant-design/pro-components';
import {
  EnvironmentOutlined,
  SolutionOutlined,
  AccountBookOutlined,
  UserOutlined,
  ProfileOutlined,
} from '@ant-design/icons';
import { Button, Pagination, Modal, Steps, Tag, Image } from 'antd';
import Mytable from '../components/item';
import { getOrderList } from '@/services/order';
import { pickBy, debounce } from 'lodash';
const { Step } = Steps;
function OrderPage() {
  const [pages, setpages] = useState(1);
  const [records, setrecords] = useState([]);
  const [total, settotal] = useState(0);
  const [open, setOpen] = useState(false);
  const [letItem, setListitem] = useState();
  enum Status {
    待付款 = 1,
    待发货 = 2,
    待收货 = 3,
    待评价 = 4,
    成功 = 5,
    失败 = 6,
  }
  enum StatusColor {
    magenta = 1,
    volcano = 2,
    gold = 3,
    lime = 4,
    green = 5,
    red = 6,
  }
  interface formList {
    orderNumber?: string;
    startTime?: string;
    Time: string;
    endTime?: string;
    status?: string;
  }
  interface pageInfotype {
    current?: number;
    size?: number;
  }
  const pageInfo: pageInfotype = {
    current: 1,
    size: 10,
  };
  const page: formList = {
    orderNumber: '',
    Time: '',
    startTime: '',
    endTime: '',
    status: '',
  };
  const onFinish = async (values: formList) => {
    page.orderNumber = values.orderNumber ? values.orderNumber : '';
    page.startTime = values.Time ? values.Time[0] : '';
    page.endTime = values.Time ? values.Time[1] : '';
    page.status = values.status ? values.status : '';
    init();
  };
  const handleChange = (page: number, pageSize: number) => {
    pageInfo.current = page;
    pageInfo.size = pageSize;
    init();
  };
  const init = () => {
    var newPage = pickBy({
      ...page,
    });
    getOrderList({ ...pageInfo, ...newPage }).then(
      ({ current, pages, records, searchCount, size, total }) => {
        pageInfo.current = current;
        settotal(total);
        setrecords(records);
        console.log(records);
      },
    );
  };
  useEffect(() => {
    init();
  }, []);
  const openmodal = (item: any) => {
    setOpen(true);
    setListitem(item);
    console.log(item);
  };
  return (
    <div className="order_box">
      <div className="order">
        <div className="from">
          <ProForm
            className="su"
            onFinish={debounce(onFinish, 2000)}
            submitter={false}
          >
            <div className="inp">
              <ProFormText name="orderNumber" placeholder="订单编号" />
              <ProFormDateRangePicker
                name="Time"
                placeholder={['开始日期', '结束日期']}
              />
              <ProFormSelect
                placeholder="请选择订单状态"
                options={[
                  {
                    value: '1',
                    label: '待付款',
                  },
                  {
                    value: '2',
                    label: '待发货',
                  },
                  {
                    value: '3',
                    label: '待收货',
                  },
                  {
                    value: '4',
                    label: '待评价',
                  },
                  {
                    value: '5',
                    label: '成功',
                  },
                  {
                    value: '6',
                    label: '失败',
                  },
                ]}
                width="xs"
                name="status"
              />
            </div>
            <div className="btn">
              <Button type="primary" htmlType="submit">
                提交
              </Button>
              <Button type="primary" htmlType="button">
                导出发货订单
              </Button>
              <Button type="primary" htmlType="button">
                导出销售记录
              </Button>
              <Button htmlType="button">清空</Button>
            </div>
          </ProForm>
        </div>
        <Mytable openmodal={openmodal} records={records}></Mytable>
        <div className="page">
          <Pagination
            size="small"
            total={total}
            showSizeChanger
            showQuickJumper
            onChange={handleChange}
          />
        </div>
        <div>
          <Modal
            title="查看"
            centered
            open={open}
            onOk={() => setOpen(false)}
            onCancel={() => setOpen(false)}
            width="80%"
            footer={false}
            maskStyle={{
              background: 'rgba(255,255,255,.8)',
            }}
          >
            <div className="modalBox">
              <div className="orderListNumber">
                订单编号：{letItem?.orderNumber}
              </div>
              <div className="orderProgress">
                <Steps current={0} labelPlacement="vertical">
                  <Step title="提交订单" />
                  <Step title="买家已付款" />
                  <Step title="卖家已发货" />
                  <Step title="买家已收货" />
                </Steps>
              </div>
              <div className="orderStatus">
                <span>订单状态:</span>
                <Tag color={StatusColor[letItem?.status]}>
                  {Status[letItem?.status]}
                </Tag>
              </div>
              <div className="order-info">
                <div className="order-info-l">
                  <div className="lr-title">
                    <ProfileOutlined
                      style={{
                        fontSize: '20px',
                        color: '#ccc',
                        marginRight: '10px',
                      }}
                    />
                    买家付款后才可以发货
                  </div>
                  <div className="Buyer">
                    <div className="icon">
                      <EnvironmentOutlined
                        style={{
                          fontSize: '20px',
                          color: '#ccc',
                        }}
                      />
                    </div>
                    <div className="Buyer-info">
                      <div>收货人：{letItem?.userAddrOrder?.receiver}</div>
                      <div>手机：{letItem?.userAddrOrder?.mobile}</div>
                      <div>
                        收货地址：
                        {letItem?.userAddrOrder?.province +
                          letItem?.userAddrOrder?.city +
                          letItem?.userAddrOrder?.area +
                          letItem?.userAddrOrder?.addr}
                      </div>
                      <div className="fp">
                        <span>
                          <AccountBookOutlined
                            style={{
                              fontSize: '20px',
                              color: '#ccc',
                            }}
                          />
                        </span>
                        不开发票
                      </div>
                    </div>
                  </div>
                </div>
                <div className="order-info-r">
                  <div className="lr-title">
                    <UserOutlined
                      style={{
                        fontSize: '20px',
                        color: '#ccc',
                        marginRight: '10px',
                      }}
                    />
                    买家:
                  </div>
                  <div className="Seller">
                    <div className="icon">
                      <SolutionOutlined
                        style={{
                          fontSize: '20px',
                          color: '#ccc',
                        }}
                      />
                    </div>
                    <div>买家备注：{letItem?.remarks}</div>
                  </div>
                </div>
              </div>
              <div className="table">
                <table>
                  <tr>
                    <td>商品</td>
                    <td>单价</td>
                    <td>数量</td>
                    <td>总价</td>
                  </tr>
                  {letItem?.orderItems &&
                    letItem?.orderItems.map((item) => {
                      return (
                        <tr>
                          <td>
                            <Image
                              id="pic"
                              src={'	https://img.mall4j.com/' + item.pic}
                              alt=""
                            />
                          </td>
                          <td>{item.price}</td>
                          <td>{item.prodCount}</td>
                          <td>{item.productTotalAmount}</td>
                        </tr>
                      );
                    })}
                </table>
                <div className="item-info">
                  <div className="infoitem">
                    商品总价:{letItem?.actualTotal}
                  </div>
                  <div className="infoitem">
                    应付金额:{letItem?.actualTotal}
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
}

export default OrderPage;
