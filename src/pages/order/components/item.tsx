import React from 'react';
import './item.less';
import { Image } from 'antd';
interface MyPorps {
  records: any;
  openmodal: Function;
}
function item(props: MyPorps) {
  const arr: any = props.records && props.records;
  enum Status {
    待付款 = 1,
    待发货 = 2,
    待收货 = 3,
    待评价 = 4,
    成功 = 5,
    失败 = 6,
  }
  enum isPayed {
    手动代付 = 0,
    微信支付 = 1,
  }
  return (
    <>
      <div className="th">
        <div>
          <span>商品</span>
        </div>
        <div>成交单价/购买数量</div>
        <div>实付金额</div>
        <div>支付方式</div>
        <div>订单状态</div>
        <div>操作</div>
      </div>
      {arr &&
        arr.map((item: any) => {
          return (
            <div className="table" key={item.orderItems.orderId}>
              <div className="orderinfor">
                订单编号:{item.orderItems[0].orderNumber}&nbsp;下单时间:
                {item.createTime}
              </div>
              <div className="main">
                <div>
                  <span id="shangpin">
                    {item.orderItems &&
                      item.orderItems.map((item) => {
                        return (
                          <div id="shangpin-item">
                            <div className="pic_box">
                              <Image
                                id="pic"
                                src={'	https://img.mall4j.com/' + item.pic}
                                alt=""
                              />
                              <div className="pic_box_r">
                                <p>{item.prodName}</p>
                                <p>{item.skuName}</p>
                              </div>
                            </div>
                            <div className="price1">
                              <div>{item.price}￥</div>
                              <div>×{item.prodCount}</div>
                            </div>
                          </div>
                        );
                      })}
                  </span>
                </div>
                <div id="productTotalAmount">
                  <div>￥{item.actualTotal}</div>
                  <div>共{item.productNums}件</div>
                </div>
                <div>{isPayed[item.isPayed]}</div>
                <div>{Status[item.status]}</div>
                <div>
                  <a onClick={() => props.openmodal(item)}>查看</a>
                </div>
              </div>
              <div className="remarks">备注:{item.remarks}</div>
            </div>
          );
        })}
    </>
  );
}

export default item;
