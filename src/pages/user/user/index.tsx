import styles from './index.less';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { editVip, showVip } from '@/services/user';
import { getVipList } from '@/services/vip';
import {
  Image,
  Tag,
  Select,
  Button,
  Modal,
  Form,
  Radio,
  Input,
  InputNumber,
} from 'antd';
import {
  ToolOutlined,
  SyncOutlined,
  SearchOutlined,
  AppstoreOutlined,
} from '@ant-design/icons';
import ColumnsTransfer from '@/components/columnsTransfer';
import { useState, useRef } from 'react';

const userindex = () => {
  const [flag, setflag] = useState(true);
  const { Option } = Select;
  const { TextArea } = Input;
  const editForm = useRef(null);
  const ref = useRef(null);
  const [titles, settitles] = useState(''); //表格标题
  const [ItemsId, setItemsId] = useState(''); //编辑id
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [records, setRecord] = useState(''); //所有数据

  type GithubIssueItem = {
    notColumnShow?: boolean;
    url: string;
    id: number;
    number: number;
    title: string;
    pic: string;
    status: number;
    labels: {
      name: string;
      color: string;
    }[];
    state: string;
  };
  enum Status {
    禁用 = 0,
    正常 = 1,
  }
  const columns: ProColumns<GithubIssueItem>[] = [
    {
      title: '用户昵称',
      width: 125,
      align: 'center',
      dataIndex: 'nickName',
    },
    {
      title: '用户头像',
      width: 230,
      align: 'center',
      dataIndex: 'pic',
      search: false,
      render(text, record, index) {
        console.log(record, 'record');
        return (
          <Image
            width={100}
            height={100}
            src={record.pic}
            fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
          />
        );
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: 125,
      align: 'center',
      render(text, record, index) {
        if (record.status) {
          return <Tag className={styles.open}>{Status[record.status]}</Tag>;
        } else {
          return <Tag className={styles.close}>{Status[record.status]}</Tag>;
        }
      },
      renderFormItem(item, options, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '注册时间',
      dataIndex: 'userRegtime',
      width: 230,
      align: 'center',
      search: false,
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      notColumnShow: true,
      render: (text, record, index) => [
        <Button
          key="link"
          type="primary"
          className={styles.btnML}
          onClick={() => {
            editItem(record);
          }}
        >
          <ToolOutlined />
          编辑
        </Button>,
      ],
    },
  ];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getVipList({
      ...arg,
    });
    console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options: any) => {
    // console.log(options);
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //点击X
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //点击确定
  const handleOk = () => {
    setIsModalOpen(false);
  };
  // 修改回显
  const editItem = async (record: any) => {
    setRecord(record);
    console.log('record', record);
    const res = await showVip(record.userId);
    console.log(res, '888888');
    setIsModalOpen(true);
    setItemsId(record.userId);
    editForm.current.setFieldsValue(res);
  };

  //点击编辑的表单
  const onFinish = async (values: any) => {
    console.log(values, 'values');
    const res = await editVip({ userId: ItemsId, ...values });
    ref.current.reload();
    editForm.current.resetFields(); //请空
  };

  return (
    <div>
      <Modal
        title="修改"
        open={isModalOpen}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          ref={editForm}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item label="用户头像" name="img">
            <Image src={records.pic}></Image>
          </Form.Item>
          <Form.Item label="用户昵称" name="nickName">
            <Input disabled />
          </Form.Item>
          <Form.Item label="状态" name="status">
            <Radio.Group>
              <Radio value={0}> 禁用 </Radio>
              <Radio value={1}> 正常 </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 15, span: 16 }}>
            <Button onClick={() => handleCancel()} className={styles.btnML}>
              取消
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => handleOk()}
              className={styles.btnML}
            >
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <ProTable
        search={flag}
        rowKey="userId"
        actionRef={ref}
        toolbar={{
          className: 'toolbar1',
          settings: [
            {
              icon: <SyncOutlined />,
              tooltip: '刷新',
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
        pagination={{
          pageSize: 5,
        }}
        columns={columnsConfig}
        request={request}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default userindex;
