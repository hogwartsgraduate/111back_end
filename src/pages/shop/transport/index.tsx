import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import { getTransportList, delTransportList } from '../../../services/spec'; //接口
import { ProTable, TableDropdown } from '@ant-design/pro-components'; //引入 高级表格
import type { ActionType, ProColumns } from '@ant-design/pro-components'; //类型
import {
  Tag,
  Button,
  Modal,
  Input,
  Radio,
  Form,
  message,
  InputNumber,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  MenuFoldOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { isArray } from 'lodash';
// ____________运费
interface ProdTableColumns {
  // title: string;
  notColumnShow?: boolean;
}

const transportindex = () => {
  const ref = useRef(null); //获取表格实例
  const formRef = useRef(null);
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState<boolean>(false);
  const [title, setTitle] = useState('新增');

  // 定义表头
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '模板名称',
      dataIndex: 'transName',
      key: 'transName',
      align: 'center',
      width: 1000,
    },
    {
      title: '操作',
      search: false,
      valueType: 'option',
      key: 'option',
      align: 'center',
      // notColumnShow: true, ///  是否在显隐弹框里面显示可以操作
      render(text, record, index) {
        return (
          <div>
            <Button
              key="editBtn"
              icon={<EditOutlined />}
              className={styles.editBtn}
              type="primary"
              onClick={() => editBtn(record)}
            >
              修改
            </Button>
            <Button
              key="delBtn"
              icon={<DeleteOutlined />}
              type="primary"
              danger
              onClick={() => delBtn(record)}
            >
              删除
            </Button>
          </div>
        );
      },
    },
  ];
  // 请求
  const request = async (arg: any) => {
    console.log(arg, 'params');

    arg.size = arg.pageSize;
    // delete arg.pageSize;
    console.log({ ...arg }, 'arg');

    const data = await getTransportList({ ...arg });
    console.log(data, '数据');
    const { records, total } = data;
    return {
      data: records,
      success: true,
      total: total,
    };
  };
  // ____________
  useEffect(() => {
    console.log('运费模板');
  }, []);
  // 方法——————
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };
  //
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  // 点击新增
  const handleVisible = () => {
    console.log('点击新增,101');
    setVisible(true);
  };
  //
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  // 删除事件
  const delBtn = (obj: any) => {
    console.log(obj, 'del');
    let id: Array<number> = [];
    if (isArray(obj.transportId)) {
      console.log('数组');
    } else {
      console.log('不是数组');
      id.push(obj.transportId);
    }
    Modal.confirm({
      title: '提示',
      content: '确定进行删除操作？',
      okText: '确定',
      cancelText: '取消',
      onOk: async () => {
        const res = await delTransportList(id);
        message.success('操作成功');
        //  刷新
        ref.current?.reload();
      },
    });
  };
  // 编辑点击 事件
  const editBtn = async (obj: any) => {
    setVisible(true);
    setTitle('编辑');
    const res = await getTransportList(obj.id);
    // setRecords(res); //拿到编辑那一行的所有值
    formRef.current?.setFieldsValue(obj);
  };
  const handleOnFinish = () => {
    console.log('提交表单');
  };
  const hasSelected = selectedRowKeys.length > 0;
  // ____________
  return (
    <div className={styles.title}>
      <ProTable
        rowSelection={rowSelection}
        pagination={{
          pageSize: 10,
          onChange: (page) => console.log(page, 'page'),
        }}
        columns={columns}
        request={request}
        rowKey="transportId"
        actionRef={ref}
        toolbar={{
          // className: classNames(styles.myToolbar),
          subTitle: [
            <Button onClick={handleVisible} key="add" type="primary">
              +新增
            </Button>,
            <Button
              type="primary"
              danger
              onClick={start}
              disabled={!hasSelected}
              loading={loading}
            >
              批量删除
            </Button>,
          ],
        }}
      />
      <Modal
        title="新增"
        open={visible}
        onCancel={() => {
          setVisible(false);
        }}
        footer={null}
      >
        <Form ref={formRef} onFinish={handleOnFinish}>
          <Form.Item
            name="transName"
            label="模板名称"
            rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="模板类型"
            rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}
          >
            <Radio.Group>
              <Radio value="apple"> 买家承担运费 </Radio>
              <Radio value="pear"> 卖家包邮 </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item
            label="收费方式"
            rules={[
              {
                required: true,
                message: '不能为空!',
              },
            ]}
          >
            <Radio.Group>
              <Radio value="apple"> 按件数 </Radio>
              <Radio value="pea"> 按重量 </Radio>
              <Radio value="pear"> 按体积 </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item name="transfees">
            <table>
              <thead>
                <tr>
                  <th>可配送区域</th>
                  <th>首件(个)</th>
                  <th>运费(元)</th>
                  <th>续件(个)</th>
                  <th>续费(元)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>所有地区</td>
                </tr>
              </tbody>
            </table>

            <Form>
              <div className={styles.tables}>
                <Form.Item name=" firstPiece">
                  <InputNumber min={0} />
                </Form.Item>
                <Form.Item name="firstFee">
                  <InputNumber min={0} />
                </Form.Item>
                <Form.Item name="continuousPiece">
                  <InputNumber min={0} />
                </Form.Item>

                <Form.Item name="continuousFee">
                  <InputNumber min={0} />
                </Form.Item>
              </div>
            </Form>
          </Form.Item>

          <Form.Item>
            <Button onClick={() => setVisible(false)}>取消</Button>
            <Button htmlType="submit" type="primary">
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default transportindex;
