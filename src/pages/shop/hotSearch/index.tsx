import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import {
  gethotSearch,
  addHotSearch,
  editHotSearch,
  showHotSearch,
  delHotItem,
} from '@/services/hotSearch';
import {
  Tag,
  Select,
  Button,
  Modal,
  Form,
  Input,
  Radio,
  InputNumber,
} from 'antd';
import {
  ToolOutlined,
  DeleteOutlined,
  SyncOutlined,
  SearchOutlined,
  AppstoreOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import ColumnsTransfer from '@/components/columnsTransfer';
import { useState, useRef } from 'react';

//热搜函数式组件
const hotSearchindex = () => {
  const [flag, setflag] = useState(true);
  //表格列数据
  const columns: ProColumns<GithubIssueItem>[] = [
    {
      title: '热搜标题',
      width: 125,
      align: 'center',
      dataIndex: 'title',
    },
    {
      title: '热搜内容',
      dataIndex: 'content',
      width: 125,
      align: 'center',
    },
    {
      title: '录入时间',
      dataIndex: 'recDate',
      width: 230,
      align: 'center',
      search: false,
    },
    {
      title: '顺序',
      dataIndex: 'seq',
      width: 125,
      search: false,
      align: 'center',
    },
    {
      title: '启用状态',
      dataIndex: 'status',
      width: 125,
      align: 'center',
      render(text, record, index) {
        if (record.status) {
          return <Tag className={styles.open}>{Status[record.status]}</Tag>;
        } else {
          return <Tag className={styles.close}>{Status[record.status]}</Tag>;
        }
      },
      renderFormItem(item, options, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['未启用']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['启用']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      notColumnShow: true,
      render: (text, record, index) => [
        <Button
          key="link"
          type="primary"
          className={styles.btnML}
          onClick={() => {
            editItem(record);
          }}
        >
          <ToolOutlined />
          修改
        </Button>,
        <Button
          key="linkdel"
          type="primary"
          danger
          onClick={() => delSwiperImg(record)}
        >
          <DeleteOutlined />
          删除
        </Button>,
      ],
    },
  ];
  const editForm = useRef(null);

  const ref = useRef(null);
  //新增弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  //显隐弹框
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [titles, settitles] = useState(''); //表格标题
  const [ItemsId, setItemsId] = useState(''); //编辑id
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]); //批量多选框
  //表格数据类型
  type GithubIssueItem = {
    notColumnShow?: boolean;
    url: string;
    id: number;
    number: number;
    title: string;
    imgUrl: string;
    status: number;
    labels: {
      name: string;
      color: string;
    }[];
    state: string;
  };
  //状态
  enum Status {
    未启用 = 0,
    启用 = 1,
  }
  const { Option } = Select;
  const { TextArea } = Input;

  //单项删除
  const { confirm } = Modal;
  const delSwiperImg = (record: any) => {
    confirm({
      title: '提示',
      content: <p> 确定进行[删除]操作？</p>,
      onOk() {
        delHotItem([record.hotSearchId]);
        ref.current.reload();
      },
    });
  };

  // 修改回显
  const editItem = async (record: any) => {
    // console.log('record',record);
    const res = await showHotSearch(record.hotSearchId);
    console.log(res, '888888');
    setIsModalOpen(true);
    settitles('修改');
    setItemsId(record.hotSearchId);
    editForm.current.setFieldsValue(res);
  };

  //request请求
  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await gethotSearch({
      ...arg,
    });
    console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  //改变显示隐藏
  const handleShowChange = (options: any) => {
    // console.log(options);
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //点击新增按钮
  const addSearch = () => {
    setIsModalOpen(true);
    settitles('新增');
  };
  //点击X
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //点击确定
  const handleOk = () => {
    setIsModalOpen(false);
  };
  //点击新增编辑的表单
  const onFinish = async (values: any) => {
    if (titles === '新增') {
      const res = await addHotSearch({ ...values });
    } else {
      const res = await editHotSearch({ hotSearchId: ItemsId, ...values });
    }
    ref.current.reload();
    editForm.current.resetFields(); //请空
  };
  //多选
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log(newSelectedRowKeys); //选中的ID
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  //多项删除
  const delAll = (record: any) => {
    confirm({
      title: '提示',
      content: <p> 确定进行[删除]操作？</p>,
      onOk() {
        delHotItem(selectedRowKeys);
        ref.current.reload();
      },
    });
  };

  //返回
  return (
    <div>
      <Modal
        title={titles}
        open={isModalOpen}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          ref={editForm}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item
            label="标题"
            name="title"
            rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="内容" name="content">
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item label="排序号">
            <Form.Item name="seq" noStyle>
              <InputNumber min={0} />
            </Form.Item>
          </Form.Item>
          <Form.Item label="状态" name="status">
            <Radio.Group>
              <Radio value="0"> 下线 </Radio>
              <Radio value="1"> 正常 </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 15, span: 16 }}>
            <Button onClick={() => handleCancel()} className={styles.btnML}>
              取消
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => handleOk()}
              className={styles.btnML}
            >
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <ProTable
        search={flag}
        actionRef={ref}
        rowKey="hotSearchId"
        rowSelection={rowSelection}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="link"
              type="primary"
              className={styles.btnML}
              onClick={addSearch}
            >
              <PlusOutlined />
              新增
            </Button>,
            <Button key="links" type="primary" danger onClick={delAll}>
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: <SyncOutlined />,
              tooltip: '刷新',
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
        pagination={{
          pageSize: 5,
        }}
        columns={columnsConfig}
        request={request}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default hotSearchindex;
