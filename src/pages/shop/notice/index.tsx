import { getNoticeList, delNoticeList } from '../../../services/spec';
import { ProTable, ProColumns } from '@ant-design/pro-components'; //引入 高级表格
import type { ActionType } from '@ant-design/pro-components'; //类型
import {
  Tag,
  Select,
  Button,
  Modal,
  message,
  Form,
  Radio,
  Input,
  Popconfirm,
} from 'antd';
import classNames from 'classnames';
import {
  MenuFoldOutlined,
  PlusOutlined,
  SettingOutlined,
  EditOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import ColumnsTransfer from '@/components/columnsTransfer';
import { useState, useRef } from 'react';
import styles from './index.less';
// ....................

interface ProdTableColumns {
  // title: string;
  notColumnShow?: boolean;
}

interface ProdTableColumns {
  // title: string;
  notColumnShow?: boolean;
}

enum Status {
  撤销 = 0,
  公布 = 1,
}

enum IsTop {
  否 = 0,
  是 = 1,
}

// 删除事件
const delBtn = async (record) => {
  try {
    const res = await delNoticeList(record.id);
    message.success('删除成功');
    formRef.current.reload();
  } catch (error) {
    message.error('删除失败');
  }
};

// 组件部分
const noticeindex = () => {
  const ref = useRef(null); //获取表格实例
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [visible, setVisible] = useState<boolean>(false);
  const formRef = useRef(null);
  const [title, setTitle] = useState('新增');
  const [records, setRecords] = useState({});
  const { confirm } = Modal;

  // 定义表头
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '序号',
      align: 'center',
      render(text, record, index) {
        return <p>{index + 1}</p>;
      },
      search: false,
    },
    {
      title: '公告内容',
      align: 'center',
      dataIndex: 'title',
    },
    {
      title: '状态',
      align: 'center',
      dataIndex: 'status',
      render(text, record, index) {
        if (record.status) {
          return <Tag color="blue">{Status[record.status]}</Tag>;
        } else {
          return <Tag color="red">{Status[record.status]}</Tag>;
        }
      },
      // 下拉框搜索
      renderFormItem(item, options, form) {
        return (
          <Select placeholder="状态">
            <Select.Option value={Status['撤销']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['公布']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '是否置顶',
      align: 'center',
      dataIndex: 'isTop',
      render(text, record, index) {
        return <Tag color="blue">{IsTop[record.isTop]}</Tag>;
      },
      renderFormItem() {
        return (
          <Select placeholder="是否置顶">
            <Select.Option value={IsTop['否']}>{IsTop[0]}</Select.Option>
            <Select.Option value={IsTop['是']}>{IsTop[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      notColumnShow: true, ///  是否在显隐弹框里面显示可以操作
      render(text, record, index) {
        return (
          <div>
            <Button
              icon={<EditOutlined />}
              type="primary"
              onClick={() => editProdTag(record)}
            >
              修改
            </Button>

            <Popconfirm
              title="确定进行删除操作?"
              onConfirm={() => handleConfirm(record)}
              okText="确定"
              cancelText="取消"
            >
              <Button icon={<DeleteOutlined />} type="primary" danger>
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  // 删除
  const handleConfirm = async (record: any) => {
    try {
      const res = await delNoticeList(record.id);
      message.success('删除成功');
      formRef.current.reload();
    } catch (error) {
      message.error('删除失败');
    }
  };
  // 请求方法
  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getNoticeList({
      ...arg,
    });
    console.log(records, '???');

    return {
      data: records,
      success: true,
      total,
    };
  };

  const handleShowChange = (options) => {
    console.log('183');

    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //
  const setShowVisible = () => {
    setTitle('新增');
    setVisible(true);
  };

  // 编辑弹框 的事件
  const handleOnFinish = () => {
    console.log('123');
  };

  const handleCancel = () => {
    console.log('取消');
  };

  // 点击【修改】，数据回显
  const editProdTag = async (obj: any) => {
    console.log(obj, '2.09');

    setVisible(true);
    setTitle('编辑');
    const res = await getNoticeList(obj.id);
    setRecords(res); //拿到编辑那一行的所有值
    formRef.current?.setFieldsValue(obj);
  };

  return (
    <div>
      <div>
        <ProTable
          pagination={{
            pageSize: 10,
          }}
          columns={columns}
          request={request}
          actionRef={formRef}
          rowKey="id"
          toolbar={{
            className: classNames(styles.myToolbar),
            subTitle: [
              <Button onClick={setShowVisible} key="add" type="primary">
                +新增
              </Button>,
            ],
            settings: [
              {
                icon: <MenuFoldOutlined />,
                tooltip: '显隐',
                key: 'show',
                onClick: () => {
                  setTransferOpen(true);
                },
              },
              {
                icon: <SettingOutlined />,
                tooltip: '设置',
              },
              {
                icon: <SettingOutlined />,
                tooltip: '设置',
              },
            ],
          }}
        />

        <ColumnsTransfer
          columns={columns}
          show={transferOpen}
          onShowChange={handleShowChange}
          modalConfig={{
            title: '多选',
            onCancel: () => {
              setTransferOpen(false);
            },
          }}
        />

        <Modal
          title={title}
          open={visible}
          onCancel={() => {
            setVisible(false);
          }}
          footer={null}
        >
          <Form ref={formRef} onFinish={handleOnFinish}>
            <Form.Item label="公告标题" name="title">
              <Input />
            </Form.Item>
            <Form.Item label="状态" name="status">
              <Radio.Group>
                <Radio value={Status['公布']}> {Status[1]} </Radio>
                <Radio value={Status['撤销']}> {Status[0]} </Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item label="指定" name="isTop">
              <Radio.Group>
                <Radio value={1}> 是 </Radio>
                <Radio value={0}> 否 </Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item>
              <Button onClick={handleCancel}>取消</Button>
              <Button htmlType="submit" type="primary">
                确定
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </div>
  );
};

export default noticeindex;
