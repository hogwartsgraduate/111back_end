import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import {
  getPickAddrList,
  PickAddrDel,
  pickAddrPut,
  getAddress,
} from '../../../services/spec';
import {
  ProColumns,
  ProTable,
  ModalForm,
  ProForm,
  ProFormSelect,
  ProFormText,
  ProFormFieldSet,
} from '@ant-design/pro-components';
// import { Tag, Select, Button, Modal, message, Form, Input, Cascader } from 'antd';
// import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
// // ____________自提点

import {
  Button,
  Form,
  message,
  Popconfirm,
  Modal,
  Input,
  Radio,
  Space,
  Switch,
} from 'antd';
import type { TableRowSelection } from 'antd/es/table/interface';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  ReloadOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import classNames from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';

interface GithubIssueItem {
  title: string;
}

interface DataType {
  key: React.ReactNode;
  name: string;
  age: number;
  address: string;
  children?: DataType[];
}

enum State {
  正常 = 1,
  禁用 = 0,
}

const handleChange = (value: string) => {
  console.log(`selected ${value}`);
};

const cancel = (e: React.MouseEvent<HTMLElement>) => {
  console.log(e);
  message.error('取消删除');
};

const pickAddrindex = () => {
  let Id = [];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);

  const confirm = async (e: any) => {
    // console.log(Id);
    await pickDel(Id);
    message.success('已成功删除');
    // init();
    await request({
      current: 1,
      size: 10,
    });
    addProdFormRef.current.reload(); //刷新页面
    form.resetFields(); //重置数据
  };

  const columns: ProColumns<GithubIssueItem>[] = [
    {
      title: <Space align="center" style={{ marginBottom: 16 }}></Space>,
      render(text, record, index) {
        return (
          <div>
            <Space align="center" style={{ marginBottom: 16 }}></Space>
          </div>
        );
      },
      hideInSearch: true,
    },
    {
      title: '自提点名称',
      dataIndex: 'addrName',
    },
    {
      title: '手机号',
      hideInSearch: true,
      dataIndex: 'mobile',
    },
    {
      title: '省份',
      dataIndex: 'province',
      hideInSearch: true,
    },
    {
      title: '城市',
      dataIndex: 'city',
      hideInSearch: true,
    },
    {
      title: '地址',
      dataIndex: 'addr',
      hideInSearch: true,
    },

    {
      title: '操作',
      hideInSearch: true,

      render(text, record, index) {
        return (
          <div>
            <Button type="primary">
              <EditOutlined /> 编辑
            </Button>
            <Popconfirm
              onClick={() => {
                Id.push(record.addrId);
              }}
              title="确定要删除吗"
              onConfirm={confirm}
              onCancel={cancel}
              okText="确定"
              cancelText="取消"
            >
              <Button type="primary" danger>
                <DeleteOutlined /> 删除
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  const [form] = Form.useForm<{ name: string; company: string }>();

  const [checkStrictly, setCheckStrictly] = useState(false);
  const rowSelection: TableRowSelection<DataType> = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    onSelect: (record, selected, selectedRows) => {
      console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      console.log(selected, selectedRows, changeRows);
    },
  };

  const init = async () => {
    const res = await getPickAddrList({
      current: 1,
      size: 10,
    });
    // console.log(res);
  };

  const setRequest = async () => {
    await request({
      current: 1,
      size: 10,
    });
    console.log('刷新数据');
  };

  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    ('');
    setColumnsConfig([...arr]);
  };

  useEffect(() => {
    init();
  }, []);

  const request = async (arg) => {
    console.log(arg);
    arg.size = arg.pageSize;
    delete arg.pageSize;

    const { records, total } = await getPickAddrList({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  return (
    <div>
      <ProTable
        rowKey="addrId"
        pagination={{
          pageSize: 10,
        }}
        // toolBarRender={toolBarRender}
        rowSelection={{ ...rowSelection, checkStrictly }}
        toolbar={{
          className: classNames(styles.myToolbar),
          subTitle: [
            <div>
              <ModalForm<{
                name: string;
                company: string;
              }>
                title="新建"
                trigger={
                  <Button type="primary">
                    <PlusOutlined />
                    新建
                  </Button>
                }
                form={form}
                autoFocusFirstInput
                modalProps={{
                  destroyOnClose: true,
                  onCancel: () => console.log('run'),
                }}
                submitTimeout={2000}
                onFinish={async (values) => {
                  console.log(values);
                  message.success('提交成功');
                  return true;
                }}
              >
                <ProForm.Group>
                  <ProFormText
                    width="md"
                    name="name"
                    label="名称"
                    placeholder="请输入名称"
                    rules={[{ required: true, message: '自提点名称不能为空' }]}
                  />
                </ProForm.Group>
                <ProFormFieldSet name="addr" label="省份">
                  <ProFormSelect
                    request={async (params) => {
                      let list: any = [];
                      let res = await pickList('0');
                      res.forEach((item: any, index: any) => {
                        list.push({ value: item.areaName });
                      });
                      return list;
                    }}
                  />
                  <ProFormSelect
                    valueEnum={{
                      changfeng: '无数据',
                    }}
                  />
                  <ProFormSelect
                    valueEnum={{
                      changfeng: '无数据',
                    }}
                  />
                </ProFormFieldSet>

                <ProForm.Group>
                  <ProFormText
                    width="md"
                    name="city"
                    label="地址"
                    placeholder="请输入地址"
                    rules={[{ required: true, message: '地址名称不能为空' }]}
                  />
                </ProForm.Group>

                <ProForm.Group>
                  <ProFormText
                    width="md"
                    name="model"
                    label="手机号"
                    placeholder="请输入手机号"
                    rules={[{ required: true, message: '手机号不能为空' }]}
                  />
                </ProForm.Group>
              </ModalForm>
              <Button
                key="dels"
                type="primary"
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  alert('del');
                }}
              >
                批量删除
              </Button>
            </div>,
          ],
          settings: [
            {
              icon: <ReloadOutlined />,
              tooltip: '刷新',
              key: 'request',
              onClick: () => {
                setRequest();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '设置',
            },
          ],
        }}
        columns={columns}
        request={request}
      />

      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default pickAddrindex;
