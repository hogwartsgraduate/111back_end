import styles from './index.less';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { getIndexImages, DeleteImg } from '@/services/indexImg';
import {
  Image,
  Tag,
  Select,
  Button,
  Table,
  Modal,
  Form,
  Input,
  Radio,
  Upload,
} from 'antd';
import React, { createContext } from 'react';
import {
  ToolOutlined,
  DeleteOutlined,
  SyncOutlined,
  SearchOutlined,
  AppstoreOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import ColumnsTransfer from '@/components/columnsTransfer';
import { useState, useRef } from 'react';
const ReachableContext = createContext<string | null>(null);
const UnreachableContext = createContext<string | null>(null);
type GithubIssueItem = {
  notColumnShow?: boolean;
  url: string;
  id: number;
  number: number;
  title: string;
  imgUrl: string;
  status: number;
  labels: {
    name: string;
    color: string;
  }[];
  state: string;
};
enum Status {
  禁用 = 0,
  正常 = 1,
}
//列表数据
const columns: ProColumns<GithubIssueItem>[] = [
  {
    title: '轮播图片',
    width: 230,
    align: 'center',
    dataIndex: 'imgUrl',
    search: false,
    render(text, record, index) {
      console.log(record, 'record');
      return (
        <Image
          width={100}
          height={100}
          src={`https://img.mall4j.com/${record.imgUrl}`}
          fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
        />
      );
    },
  },
  {
    title: '顺序',
    dataIndex: 'seq',
    search: false,
    width: 230,
    align: 'center',
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 230,
    align: 'center',
    render(text, record, index) {
      return <Tag>{Status[record.status]}</Tag>;
    },
    renderFormItem(item, options, form) {
      const handleStatusChange = (value: number) => {
        form.setFieldValue('status', value);
      };
      return (
        <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
          <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
          <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
        </Select>
      );
    },
  },
  {
    title: '操作',
    width: 230,
    search: false,
    notColumnShow: true,
    align: 'center',
    render: (text, record, index) => [
      <Button key="link" type="primary" className={styles.btnML}>
        <ToolOutlined />
        编辑
      </Button>,
      <Button
        key="linkdel"
        type="primary"
        danger
        onClick={() => delSwiperImg(record)}
      >
        <DeleteOutlined />
        删除
      </Button>,
    ],
  },
];
const { confirm } = Modal;

const indexImgindex = () => {
  const [flag, setflag] = useState(true);
  const editForm = useRef(null);
  const ref = useRef(null);
  //单项删除
  const delSwiperImg = (record: any) => {
    confirm({
      title: '提示',
      content: <p> 确定进行[删除]操作？</p>,
      onOk() {
        DeleteImg([record.imgId]);
        ref.current.reload();
      },
    });
  };
  //显隐弹框
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  //新增弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  //请求
  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getIndexImages({
      ...arg,
    });
    console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  //改变显隐的状态值
  const handleShowChange = (options) => {
    // console.log(options);
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //点击新增按钮
  const addImg = () => {
    setIsModalOpen(true);
  };
  //点击X
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //点击确定
  const handleOk = () => {
    setIsModalOpen(false);
    // 验证输入的内容是否合法
  };

  return (
    <div>
      <Modal
        title="新增"
        open={isModalOpen}
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <Form
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          ref={editForm}
        >
          <Form.Item
            label="轮播图片"
            valuePropName="fileList"
            rules={[{ required: true, message: '轮播图片不能为空' }]}
          >
            <Upload action="/upload.do" listType="picture-card">
              <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}></div>
              </div>
            </Upload>
          </Form.Item>
          <Form.Item label="顺序">
            <Input />
          </Form.Item>
          <Form.Item label="状态">
            <Radio.Group>
              <Radio value="apple"> 禁用 </Radio>
              <Radio value="pear"> 正常 </Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="类型">
            <Radio.Group>
              <Radio value="apple"> 无 </Radio>
              <Radio value="pear"> 商品 </Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Modal>
      <ProTable
        actionRef={ref}
        search={flag}
        rowKey="imgId"
        rowSelection={{
          alwaysShowAlert: true,
          // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
          // 注释该行则默认不显示下拉选项
          // selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
          // defaultSelectedRowKeys: [1],
        }}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="link"
              type="primary"
              className={styles.btnML}
              onClick={addImg}
            >
              <PlusOutlined />
              新增
            </Button>,
            <Button key="links" type="primary" danger>
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: <SyncOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setflag(!flag);
              },
            },
          ],
        }}
        pagination={{
          pageSize: 5,
        }}
        columns={columnsConfig}
        request={request}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default indexImgindex;
