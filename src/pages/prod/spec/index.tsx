import styles from './index.less';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import {
  Button,
  DatePicker,
  Space,
  Table,
  Tag,
  Modal,
  Form,
  Input,
  message,
} from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
// import { getGuigeList, addGuigeList } from '@/services/store';
import ColumnsTransfer from '@/components/columnsTransfer';
import { useState, useRef, useEffect } from 'react';
import classNames from 'classnames';
import {
  PlusOutlined,
  MenuFoldOutlined,
  SettingOutlined,
  MinusCircleOutlined,
  SearchOutlined,
  SyncOutlined,
  CloseOutlined,
  SnippetsOutlined,
} from '@ant-design/icons';
import {
  ProCard,
  ProForm,
  ProFormList,
  ProFormText,
} from '@ant-design/pro-components';
import {
  getSpecList,
  addSpecList,
  delSpecList,
  editSpecList,
} from '../../../services/spec';

interface ProdTableColumns {
  title: string;
}

//
type GithubIssueItem = {
  url: string;
  id: number;
  number: number;
  title: string;
  labels: {
    name: string;
    color: string;
  }[];
  state: string;
  comments: number;
  created_at: string;
  updated_at: string;
  closed_at?: string;
};

const specindex = () => {
  //   _____________________

  const ref = useRef(null); //获取表格实例
  const editRef = useRef(null); //表单实例

  const [searchFlag, setSearchFlag] = useState(true); // 控制头部搜索显示隐藏
  const [records, setRecords] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false); //对话框

  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [visible, setVisible] = useState<Boolean>(false);
  const [title, setTitle] = useState('添加');
  const [form] = Form.useForm();

  let formRef = useRef(null);

  const { confirm } = Modal;
  //请求数据
  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getSpecList({
      ...arg,
    });
    // console.log(data,'规格管理数据---------');

    return {
      data: records,
      success: true,
      total,
    };
  };
  // 显示
  const handleShowChange = (options: any) => {
    console.log('90');

    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  // 点击新增
  const handleVisible = () => {
    // console.log('点击新增,101');
    setTitle('新增');
    setVisible(true);
  };
  //点击 新增 取消按钮 触发
  const handleVisibleFalse = () => {
    console.log('107');
    setVisible(false);
  };
  // 点击 新增的 提交按钮 触发
  const handleFinish = async (values: any) => {
    console.log(values, '提交内容134');
    if (title === '新增') {
      await addSpecList(values);
      message.success('新增成功');
    } else {
      await editSpecList(values);
      message.success('修改成功');
    }
    //  刷新
    ref.current?.reload();
    setVisible(false);
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  };
  const formItemLayoutWithOutLabel = {
    wrapperCol: {
      xs: { span: 24, offset: 0 },
      sm: { span: 20, offset: 4 },
    },
  };

  // const App: React.FC = () => {
  //   const onFinish = (values: any) => {
  //     console.log('Received values of form:', values);
  //   };
  // };

  // 删除事件
  const delBtn = (obj: any) => {
    console.log(obj, 'del');

    Modal.confirm({
      title: '提示',
      content: '确定进行删除操作？',
      okText: '确定',
      cancelText: '取消',
      onOk: async () => {
        const res = await delSpecList(obj.propId);
        message.success('操作成功');
        //  刷新
        ref.current?.reload();
      },
    });
  };
  // 修改事件
  const editBtn = async (obj: any) => {
    console.log(obj, 'edit');
    setTitle('编辑');
    setVisible(true); // 触发 对话框
    console.log(obj.propId, 'id');
    const id: number = obj.propId;
    console.log(formRef, 'editRef');
    formRef.current?.setFieldsValue(obj);
  };

  // 定义表头数据
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '序号',
      align: 'center',
      render(text, record, index) {
        return <p>{index + 1}</p>;
      },
      search: false,
      notColumnShow: true, ///  是否在显隐弹框里面显示可以操作
    },
    {
      title: '属性名称',
      align: 'center',
      dataIndex: 'propName',
    },
    {
      title: '属性值',
      align: 'center',
      dataIndex: 'propValue',
      search: false,
      render: (_, record) => (
        <Space>
          {record?.prodPropValues.map(({ propValue, color }) => (
            <Tag color="blue" key={propValue}>
              {propValue}
            </Tag>
          ))}
        </Space>
      ),
    },
    {
      title: '操作',
      align: 'center',
      search: false,
      notColumnShow: true, ///  是否在显隐弹框里面显示可以操作
      render(text, record, index) {
        return (
          <div>
            <Button
              type="primary"
              icon={<EditOutlined />}
              onClick={() => editBtn(record)}
            >
              编辑
            </Button>
            <Button
              type="primary"
              danger
              icon={<DeleteOutlined />}
              onClick={() => delBtn(record)}
            >
              删除
            </Button>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    console.log('规格');
  }, []);

  // const actionRef = useRef<ActionType>();
  return (
    // 表格部分
    <div className={styles.title}>
      <ProTable
        pagination={{
          pageSize: 5,
        }}
        actionRef={ref} //绑定表格
        search={searchFlag}
        columns={columns}
        request={request} // 表格初始化点击查询 点击分页
        rowKey="propId"
        toolbar={{
          className: classNames(styles.myToolbar),
          subTitle: [
            <Button onClick={handleVisible} key="add" type="primary">
              +新增
            </Button>,
          ],
          settings: [
            {
              icon: (
                <Button
                  icon={<SyncOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '刷新',
              onClick: () => {
                ref.current?.reload();
              },
            },
            {
              icon: <MenuFoldOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <Button
                  icon={<SearchOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearchFlag(!searchFlag);
              },
            },
          ],
        }}
        form={{ span: 8 }}
      />

      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      {/* 弹框 */}
      <Modal
        open={visible}
        title="新增"
        onCancel={() => {
          setVisible(false);
        }}
        footer={null}
      >
        <Form
          name="form"
          ref={formRef}
          form={form}
          onFinish={handleFinish}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          className={styles.forms}
        >
          <div className={styles.divs}>
            <div className={styles.headers}>
              <Form.Item label="属性名称" name="propName">
                <Input />
              </Form.Item>
            </div>
            <div className={styles.bodys}>
              <Form.List name="prodPropValues">
                {(fields, { add, remove }, { errors }) => (
                  <>
                    {fields.map((field, index) => (
                      <Form.Item required={false} key={field.key}>
                        <Form.Item
                          {...field}
                          validateTrigger={['onChange', 'onBlur']}
                          noStyle
                          name="propValue"
                        >
                          <Input
                            placeholder="请输入内容"
                            style={{ width: '90%' }}
                          />
                        </Form.Item>

                        {fields.length > 1 ? (
                          <MinusCircleOutlined
                            className="dynamic-delete-button"
                            onClick={() => remove(field.name)}
                          />
                        ) : null}
                      </Form.Item>
                    ))}
                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => add()}
                        style={{ width: '60%' }}
                        icon={<PlusOutlined />}
                      ></Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </div>
          </div>

          <Form.Item>
            <Button onClick={handleVisibleFalse}>取消</Button>
            <Button htmlType="submit" type="primary">
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default specindex;
