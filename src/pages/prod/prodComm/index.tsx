import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import { getprodCommList } from '@/services/prod';
import { Select, Button } from 'antd';
import ColumnsTransfer from '@/components/columnsTransfer'; //穿梭框弹框

import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  SyncOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';

// 审核状态
enum Status {
  待审核 = 0,
  审核通过 = 1,
  审核未通过 = -1,
}

const prodCommindex = () => {
  const columns = [
    {
      title: '序号',
      dataIndex: 'sortN',
      search: false,
      align: 'center',
      width: '60px',
    },
    {
      title: '商品名',
      dataIndex: 'prodName',
      align: 'center',
      width: '125px',
    },
    {
      title: '用户昵称',
      dataIndex: 'nickName',
      search: false,
      align: 'center',
      width: '125px',
    },
    {
      title: '记录时间',
      search: false,
      dataIndex: 'time',
      align: 'center',
      width: '210apx',
    },
    {
      title: '回复时间',
      dataIndex: 'answerTime',
      search: false,
      align: 'center',
    },
    {
      title: '评价得分',
      dataIndex: 'core',
      search: false,
      align: 'center',
    },
    {
      title: '是否匿名',
      dataIndex: 'noName',
      search: false,
      align: 'center',
    },
    {
      title: '审核状态',
      dataIndex: 'status',
      align: 'center',
      renderFormItem(item, options, form) {
        // 选择下拉框进行搜索
        const handleOnChange = (value: number) => {
          console.log(value, 'values-------');
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder="审核状态" onChange={handleOnChange}>
            <Select.Option value={Status['待审核']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['审核通过']}>
              {Status[1]}
            </Select.Option>
            <Select.Option value={Status['审核未通过']}>
              {Status[-1]}
            </Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      notColumnShow: true, //在穿梭框的数据中不展示操作那一列
      render: () => {
        return (
          <div>
            <Button
              type="primary"
              style={{ margin: 10 }}
              icon={<EditOutlined />}
            >
              修改
            </Button>
            <Button type="primary" danger icon={<DeleteOutlined />}>
              删除
            </Button>
          </div>
        );
      },
    },
  ];

  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  const ref = useRef(null); //绑定表格
  const [searchFlag, setSearchFlag] = useState(true); //是否展示头部搜索栏

  // 更改页面视图
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getprodCommList({
      ...arg,
    });

    return {
      data: records,
      success: true,
      total,
    };
  };

  return (
    <div>
      <ProTable
        columns={columnsConfig}
        request={request}
        actionRef={ref}
        search={searchFlag}
        pagination={{
          pageSize: 20,
        }}
        toolbar={{
          settings: [
            {
              icon: (
                <Button
                  icon={<SyncOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // 刷新
                ref.current.reload();
              },
            },
            {
              icon: (
                <Button
                  icon={<AppstoreOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <Button
                  icon={<SearchOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearchFlag(!searchFlag);
              },
            },
          ],
        }}
      />
      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};

export default prodCommindex;
