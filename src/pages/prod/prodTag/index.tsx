import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import {
  getProdTagList,
  getProdTagListAdd,
  getProdTagListDel,
  getProdTagListInfo,
  getProdTagListEdit,
} from '@/services/prod'; //接口
import { Tag, Button, Select, Modal, Form, Input, Radio, message } from 'antd';
import type { ProColumns } from '@ant-design/pro-components';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  SyncOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import ColumnsTransfer from '@/components/columnsTransfer';

interface ProdTagColumns {
  title: string;
}

// 用枚举方法定义状态
enum Status {
  禁用 = 0,
  正常 = 1,
}

// 默认类型
enum Type {
  自定义类型 = 0,
  默认类型 = 1,
}

// 弹框列表样式
enum Style {
  一列一个 = 0,
  一列两个 = 1,
  一列三个 = 2,
}

const prodTagindex = () => {
  // 表格列
  const columns = [
    {
      title: '序号',
      dataIndex: 'sort',
      render(text, record, index) {
        return <span>{index + 1}</span>;
      },
      search: false,
      align: 'center',
      width: '64px',
      height: '47px',
    },
    {
      title: '标签名称',
      dataIndex: 'title',
      align: 'center',
      width: '264px',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return <Tag color={'blue'}>{Status[record.status]}</Tag>;
      },
      renderFormItem(item, options, form) {
        const handleOnChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'状态'} onChange={handleOnChange}>
            <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
      align: 'center',
      width: '261px',
    },
    {
      title: '默认类型',
      dataIndex: 'isDefault',
      render: (text, record, index) => {
        return <Tag color="blue">{Type[record.isDefault]}</Tag>;
      },
      search: false,
      align: 'center',
      width: '260px',
    },
    {
      title: '排序',
      dataIndex: 'seq',
      search: false,
      align: 'center',
      width: '261px',
    },
    {
      title: '操作',
      render: (text, record, index) => {
        return (
          <div>
            <Button
              type="primary"
              style={{ margin: 10 }}
              icon={<EditOutlined />}
              onClick={() => editProdTag(record.id)}
            >
              修改
            </Button>
            <Button
              type="primary"
              danger
              icon={<DeleteOutlined />}
              onClick={() => delProdTag(record.id)}
            >
              删除
            </Button>
          </div>
        );
      },
      search: false,
      align: 'center',
      notColumnShow: true,
    },
  ];
  const ref = useRef(null); //获取表格实例
  const [isAddModalOpen, setIsAddModalOpen] = useState<boolean>(false); //新增、编辑弹框
  const [title, setTitle] = useState('新增');
  const editRef = useRef(null); //表单实例
  const [records, setRecords] = useState({});
  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  const [searchFlag, setSearchFlag] = useState(true); // 控制头部搜索显示隐藏

  // 渲染表格
  const request = async (arg) => {
    arg.page = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getProdTagList({
      ...arg,
    });
    // console.log(records,"records-------------");

    return {
      data: records, //渲染表格数据
      success: true,
      total,
    };
  };

  // 点击【新增】弹框确定按钮
  const onFinish = async (values: any) => {
    if (title === '新增') {
      setTitle('新增');
      const res = await getProdTagListAdd(values);
      message.success('操作成功');
      editRef.current?.resetFields();
      //  刷新
      ref.current.reload();
    }
    if (title === '修改') {
      const res = await getProdTagListEdit({
        ...values,
        createTime: records.createTime,
        deleteTime: records.deleteTime,
        id: records.id,
        isDefault: records.isDefault,
        prodCount: records.prodCount,
        shopId: records.shopId,
        updateTime: records.updateTime,
      });
      // console.log(res,"修改");
      message.success('操作成功');
      ref.current.reload();
    }
  };

  // 点击【删除】 /prod/prodTag/page
  const delProdTag = (id: number) => {
    // 删除的二次提示框
    Modal.confirm({
      title: '提示',
      content: '确定进行删除操作？',
      okText: '确定',
      cancelText: '取消',
      onOk: async () => {
        const res = await getProdTagListDel(id);
        message.success('操作成功');
        //  刷新
        ref.current.reload();
      },
    });
  };

  // 点击【修改】，数据回显
  const editProdTag = async (id: number) => {
    setIsAddModalOpen(true);
    setTitle('修改');
    const res = await getProdTagListInfo(id);
    setRecords(res); //拿到编辑那一行的所有值
    editRef.current?.setFieldsValue(res);
    // console.log(editRef,"表单实例----------");
    // console.log(res,"----------");
  };

  // 穿梭框控制页面视图
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };

  return (
    <div>
      <ProTable
        pagination={{
          pageSize: 10, //每页显示条数
        }}
        rowKey="id"
        columns={columnsConfig}
        request={request} //触发场景： 表格初始，点击查询，点击分页
        bordered={true}
        actionRef={ref} //绑定表格
        search={searchFlag}
        toolbar={{
          subTitle: [
            <Button
              key="add"
              type="primary"
              onClick={() => {
                setIsAddModalOpen(true);
              }}
              icon={<PlusOutlined />}
              style={{ marginRight: 10 }}
            >
              新增
            </Button>,
          ],
          settings: [
            {
              icon: (
                <Button
                  icon={<SyncOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: (
                <Button
                  icon={<AppstoreOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <Button
                  icon={<SearchOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearchFlag(!searchFlag);
              },
            },
          ],
        }}
        form={{
          resetText: '清空',
          searchText: '搜索',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
      ></ProTable>

      {/* 新增、编辑 弹框 */}
      <Modal
        title={title}
        open={isAddModalOpen}
        footer={null}
        onCancel={() => setIsAddModalOpen(false)} //关闭弹框
      >
        <Form
          name="add"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          onFinish={onFinish}
          autoComplete="off"
          ref={editRef}
        >
          <Form.Item label="标签名称" name="title">
            <Input />
          </Form.Item>

          <Form.Item name="status" label="状态">
            <Radio.Group>
              <Radio value={Status['正常']}>正常</Radio>
              <Radio value={Status['禁用']}>禁用</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item name="style" label="列表样式">
            <Radio.Group>
              <Radio value={Style['一列一个']}> 一列一个 </Radio>
              <Radio value={Style['一列两个']}> 一列两个 </Radio>
              <Radio value={Style['一列三个']}> 一列三个 </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="排序" name="seq">
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 16, span: 8 }}>
            <Button
              htmlType="submit"
              style={{ margin: 10 }}
              onClick={() => setIsAddModalOpen(false)}
            >
              取消
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => setIsAddModalOpen(false)}
            >
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      {/* 穿梭框  */}
      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};

export default prodTagindex;
