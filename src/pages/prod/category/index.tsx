import { useRef, useState, useEffect, useMemo } from 'react';
import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import {
  getCategoryList,
  getCategoryInfo,
  getCategoryEdit,
} from '@/services/prod';
import {
  Tag,
  Image,
  Button,
  Modal,
  Form,
  Input,
  Upload,
  InputNumber,
  Radio,
  Cascader,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { message } from 'antd';

enum Status {
  正常 = 1,
  异常 = 0,
}

const categoryindex = () => {
  const columns = [
    {
      title: '分类名称',
      dataIndex: 'categoryName',
      search: false,
      align: 'center',
      width: '150px',
    },
    {
      title: '图片',
      dataIndex: 'pic',
      align: 'center',
      search: false,
      width: '305px',
      render: (text, records, index) => {
        return (
          <Image
            src={`http://img-test.gz-yami.com/${records.pic}`}
            width={230}
            height={80}
            fallback="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png?x-oss-process=image/blur,r_50,s_50/quality,q_1/resize,m_mfit,h_200,w_200"
          />
        );
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      width: '305px',
      search: false,
      render: (text, records, index) => {
        return <Tag color="blue">{Status[records.status]}</Tag>;
      },
    },
    {
      title: '排序号',
      dataIndex: 'seq',
      align: 'center',
      width: '305px',
      search: false,
      render: (text, records, index) => {
        return <div>1</div>;
      },
    },
    {
      title: '操作',
      align: 'center',
      search: false,
      width: '305px',
      render: (text, record, index) => {
        return (
          <div>
            <Button
              type="primary"
              style={{ margin: 10 }}
              onClick={() => handleEdit(record.categoryId)}
            >
              修改
            </Button>
            <Button type="primary" danger>
              删除
            </Button>
          </div>
        );
      },
    },
  ];

  const [title, setTitle] = useState('新增'); //弹框标题
  const [openModal, setOpenModal] = useState(false); //新增、编辑弹框开关
  const editFormRef = useRef(null); //绑定表格
  const [categoryItem, setCategoryItem] = useState({}); //获取点击修改的那一条数据
  const [categoryList, setCategoryList] = useState([]); ///格式化之后的数据
  const ref = useRef(null);

  // 点击【修改】数据回显
  const handleEdit = async (id: number) => {
    setOpenModal(true);
    setTitle('修改');
    const data = await getCategoryInfo(id);
    setCategoryItem(data);
    console.log(data, '点击修改那数据-----');
    editFormRef.current?.setFieldsValue(data);
  };
  // categoryId:85

  // grade:0
  // parentId:0

  // categoryName: "手机数码"
  // pic :  "2019/04/4f148d81d60941b695cb77370a073653.jpg"
  // seq : 1
  // status :  1

  // 点击【表单确定】按钮
  const onFinish = async (values) => {
    if (title === '修改') {
      const data = await getCategoryEdit({
        ...values,
        categoryId: categoryItem.categoryId,
        grade: categoryItem.grade,
        parentId: categoryItem.parentId,
        pic: categoryItem.pic,
      });
      message.success('操作成功');
      setOpenModal(false);
      // 刷新
      ref.current.reload();
    }
  };

  // 点击【删除】

  const request = async (arg) => {
    const data = await getCategoryList();
    const data1 = data.filter((item) => item.parentId === 0);
    console.log(data, '分类管理数据data--------');

    return {
      data: data1,
      success: true,
    };
  };

  return (
    <div>
      <ProTable
        columns={columns}
        request={request}
        actionRef={ref}
        rowKey="categoryId"
        bordered={true}
        pagination={{
          pageSize: 5,
        }}
        search={{
          //不显示搜索按钮
          optionRender: false,
          collapsed: false,
        }}
        toolbar={{
          subTitle: [
            <Button
              key="add"
              type="primary"
              onClick={() => setOpenModal(true)}
              icon={<PlusOutlined />}
            >
              新增
            </Button>,
          ],
          settings: ['none'],
        }}
      />

      {/* 新增、修改、编辑 弹框  */}
      <Modal
        title={title}
        open={openModal}
        onCancel={() => setOpenModal(false)}
        footer={null}
      >
        <Form
          name="categoryAdd"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          onFinish={onFinish}
          autoComplete="off"
          ref={editFormRef}
        >
          <Form.Item
            label="分类图片"
            valuePropName="pic"
            name="pic"
            rules={[{ required: true, message: '图片不能为空' }]}
          >
            <Upload
              action="https://bjwz.bwie.com/mall4w"
              listType="picture-card"
            >
              <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}></div>
              </div>
            </Upload>
          </Form.Item>

          <Form.Item
            label="分类名称"
            name="categoryName"
            rules={[{ required: true, message: '分类名称不能为空' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="上级分类" name="">
            <Cascader
              // options={dataSource}
              expandTrigger="hover"
              // displayRender={displayRender}
              // onChange={onChange}
            />
          </Form.Item>

          <Form.Item label="排序号" name="seq">
            <InputNumber min={0} />
          </Form.Item>

          <Form.Item label="状态" name="status">
            <Radio.Group>
              <Radio value={Status['异常']}>异常</Radio>
              <Radio value={Status['正常']}>正常</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 16, span: 8 }}>
            <Button
              htmlType="submit"
              style={{ margin: 10 + 'px' }}
              onClick={() => setOpenModal(false)}
            >
              取消
            </Button>
            <Button type="primary" htmlType="submit">
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default categoryindex;
