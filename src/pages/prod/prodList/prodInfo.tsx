import {
  Form,
  Input,
  Upload,
  Button,
  Radio,
  Cascader,
  Checkbox,
  Select,
  InputNumber,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './index.less';

import React, { useEffect, useRef } from 'react';
import ReactWEditor from 'wangeditor-for-react';
import {
  getShopTransportList,
  getProdSpecList,
  getAddProdTagList,
  getProdCategoryList,
} from '@/services/prod';
import { init } from '@umijs/deps/compiled/webpack';

enum Status {
  下架 = 0,
  上架 = 1,
}

const ProdInfo = () => {
  const init = async () => {
    const shopTransportData = await getShopTransportList();
    const prodSpecData = await getProdSpecList();
    const addProdTagData = await getAddProdTagList();
    const prodCategory = await getProdCategoryList();

    console.log(
      shopTransportData,
      prodSpecData,
      addProdTagData,
      prodCategory,
      '一组数据---------',
    );
  };
  useEffect(() => {
    init();
  }, []);

  const editorRef = useRef(null);
  return (
    <div>
      <Form
        name="prodListAdd"
        labelCol={{ span: 2 }}
        wrapperCol={{ span: 22 }}
        // onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item label="产品图片" valuePropName="fileList" name="pic">
          <Upload action="https://bjwz.bwie.com/mall4w" listType="picture-card">
            <div>
              <PlusOutlined />
              <div style={{ marginTop: 8 }}></div>
            </div>
          </Upload>
        </Form.Item>

        <Form.Item label="状态" name="status">
          <Radio.Group>
            <Radio value={Status['上架']}>{Status[1]}</Radio>
            <Radio value={Status['下架']}>{Status[0]}</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="产品分类" name="categoryName">
          <Cascader
            placeholder={'请选择'}
            // options={options}
            // onChange={onChange}
            changeOnSelect
            expandTrigger={'hover'}
            style={{ width: 200 + 'px' }}
          />
        </Form.Item>

        <Form.Item label="产品分组" name="title">
          <Cascader
            placeholder={'请选择'}
            // options={options}
            // onChange={onChange}
            changeOnSelect
            expandTrigger={'hover'}
            style={{ width: 240 + 'px' }}
          />
        </Form.Item>

        <Form.Item
          label="产品名称"
          name="prodName1"
          rules={[{ required: true, message: '产品名称不能为空' }]}
        >
          <Input style={{ width: 400 + 'px' }} placeholder="产品名称" />
        </Form.Item>

        <Form.Item label="产品卖点" name="brief">
          <Input.TextArea
            maxLength={100}
            placeholder="产品卖点"
            style={{ width: 400 + 'px' }}
          />
        </Form.Item>

        <Form.Item label="配送方式" name="transport">
          <Checkbox>商家配送</Checkbox>
          <Checkbox>用户自提</Checkbox>
        </Form.Item>

        <Form.Item label="商品规格" name="sku">
          <Button style={{ fontSize: 12 + 'px' }}>添加规格</Button>
        </Form.Item>

        <Form.Item label="规格名" name="sku1">
          <Select placeholder="请选择" style={{ width: 200 + 'px' }}>
            <Select.Option value="demo1">内存</Select.Option>
            <Select.Option value="demo2">颜色</Select.Option>
            <Select.Option value="demo3">框架</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="规格值" name="sku2">
          <Select placeholder="请选择" style={{ width: 200 + 'px' }}>
            <Select.Option value="demo1">内存11</Select.Option>
            <Select.Option value="demo2">颜色22</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 2, span: 22 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ marginRight: 10 + 'px' }}
          >
            确定
          </Button>
          <Button>取消</Button>
        </Form.Item>

        <Form.Item
          wrapperCol={{ offset: 2, span: 22 }}
          // label="Password"
          // name="password"
        >
          <table className="table">
            <thead>
              <tr>
                <td>销售价</td>
                <td>市场价</td>
                <td>库存</td>
                <td>商品重量(kg)</td>
                <td>商品体积(m)</td>
                <td>操作</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <InputNumber min={0.01} defaultValue={0.01} />
                </td>
                <td>
                  <InputNumber min={0.01} defaultValue={0.01} />
                </td>
                <td>
                  <InputNumber defaultValue={0} />
                </td>
                <td>
                  <Input />
                </td>
                <td>
                  <Input />
                </td>
                <td>
                  <Button type="link">禁用</Button>
                </td>
              </tr>
            </tbody>
          </table>
        </Form.Item>

        <Form.Item label="产品详情" name="username">
          <ReactWEditor ref={editorRef} style={{ width: 1100 + 'px' }} />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 2, span: 24 }}>
          <Button type="primary" htmlType="submit">
            确定
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default ProdInfo;
