import { useState, useRef } from 'react';
import styles from './index.less';
import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import { getProdList } from '@/services/prod';
import { Image, Button, Tag, Select } from 'antd';
import ColumnsTransfer from '@/components/columnsTransfer';
import { Link } from 'react-router-dom';

import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  SyncOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import { validate } from 'uuid';

interface ProdTableColumns {
  notColumnShow?: boolean;
  title: string;
}

// 产品状态
enum Status {
  未上架 = 0,
  上架,
}

const prodListindex = () => {
  const columns: ProColumns<ProdTableColumns> = [
    {
      title: '产品名字',
      dataIndex: 'prodName',
      align: 'center',
      width: '180px',
    },
    {
      title: '商品原价',
      dataIndex: 'oriPrice',
      search: false,
      align: 'center',
      width: '160px',
    },
    {
      title: '商品现价',
      dataIndex: 'price',
      search: false,
      align: 'center',
      width: '160px',
    },
    {
      title: '商品库存',
      dataIndex: 'totalStocks',
      search: false,
      align: 'center',
      width: '160px',
    },
    {
      title: '产品图片',
      dataIndex: 'pic',
      search: false,
      render: (text, record, index) => {
        return (
          <Image
            src={record.pic}
            width={140}
            fallback="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png?x-oss-process=image/blur,r_50,s_50/quality,q_1/resize,m_mfit,h_200,w_200"
          />
        );
      },
      align: 'center',
      width: '160px',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return <Tag color="blue">{Status[record.status]}</Tag>;
      },
      renderFormItem(item, options, form) {
        const handleStatusChange = (value: number) => {
          // console.log(value,"产品管理-----");
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['未上架']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['上架']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
      align: 'center',
    },
    {
      title: '操作',
      search: false,
      notColumnShow: true, //在穿梭框的数据中不展示操作那一列
      render: () => {
        return (
          <div>
            <Button
              type="primary"
              style={{ margin: 10 }}
              icon={<EditOutlined />}
            >
              修改
            </Button>
            <Button type="primary" danger icon={<DeleteOutlined />}>
              删除
            </Button>
          </div>
        );
      },
      align: 'center',
    },
  ];

  const [transferOpen, setTransferOpen] = useState<boolean>(false); //控制穿梭弹框的开关
  // 更改组件视图，定义组件视图组件状态
  const [columnsConfig, setColumnsConfig] = useState(columns);
  const ref = useRef(null); //表单刷新
  const [searchflag, setSearchFlag] = useState(true); //头部搜索栏显示隐藏

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const hasSelected = selectedRowKeys.length > 0;

  // 更改页面视图
  // ❤ 【通过触发handleShowChange事件， 重现找到穿梭框中key和表格数据中dataIndex一致，同时isShow为true的列，重新对列进行赋值，让表格视图改变】
  const handleShowChange = (options: any) => {
    //dataSource的数据options ，isShow改变状态
    // console.log(options,"options-----");
    const arr = columns.filter(
      (
        item, //columnsConfig每次改变都是变化之后的数组，所以选用外部定义的数据进行数据改变
      ) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]); //重新更改表格数据
  };

  const request = async (arg) => {
    //❤ request触发场景： 表格初始，点击查询，点击分页。request是表格自己调用的 ，不是自己手动调用的，要想实现表格刷新，需要给表格绑定【actionRef】
    arg.size = arg.pageSize;
    delete arg.pageSize; //删除arg中的pageSize属性
    const { records, total } = await getProdList({
      ...arg,
    });

    return {
      data: records,
      success: true,
      total,
    };
  };
  return (
    <div>
      <ProTable
        rowKey="prodId"
        actionRef={ref}
        search={searchflag}
        columns={columnsConfig}
        request={request}
        rowSelection={rowSelection}
        bordered={true}
        pagination={{
          pageSize: 10,
        }}
        toolbar={{
          subTitle: [
            <Link to="/prodInfo">
              <Button
                key="add"
                type="primary"
                icon={<PlusOutlined />}
                style={{ marginRight: 10 }}
              >
                新增
              </Button>
            </Link>,
            <Button
              key="delmore"
              type="primary"
              danger
              onClick={() => {
                // alert('add');
              }}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <Button
                  icon={<SyncOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // 刷新
                ref.current.reload();
              },
            },
            {
              icon: (
                <Button
                  icon={<AppstoreOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <Button
                  icon={<SearchOutlined />}
                  style={{ borderRadius: 50 + '%' }}
                ></Button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearchFlag(!searchflag);
              },
            },
          ],
        }}
        form={{
          resetText: '清空',
          searchText: '搜索',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
      ></ProTable>

      <ColumnsTransfer
        columns={columns} //整个表格的数据
        show={transferOpen}
        modalConfig={{
          //穿透
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
        onShowChange={handleShowChange}
      />
    </div>
  );
};

export default prodListindex;
